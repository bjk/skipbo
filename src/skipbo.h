/*
    Copyright (C) 2013-2016 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SKIPBO_H
#define SKIPBO_H

#define MAX_DISCARD 25
#define MAX_CMDS 100

#define NO_CARD 0
#define SKIP_BO 13

#ifdef WITH_GNUTLS
#include <gnutls/gnutls.h>
#endif

struct command_s
{
  long id;  // Unique to each command
  int cmd; // The command (see server.c)
};

struct player_s
{
  char *name;
  unsigned char hand[5];
  unsigned char stock[15]; // Upper limit
  int remaining;
  int score;

  struct {
    unsigned char cards[MAX_DISCARD]; // FIXME limit???
    int count;
  } discard[4];

  int fd;
  int id;                 // Player ID
  int ready;
  struct command_s cmds[MAX_CMDS]; // command queue
  struct command_s cmd;   // current command
  long cmd_id;            // internal unique id
  char *buf;              // read() from client
  char *result;           // pointer to buf containing the result which may be
                          // after a chat line
  int encrypt;
#ifdef WITH_GNUTLS
  gnutls_session_t ses;
#endif
};

extern struct player_s *players;
extern int stock_total;
extern int n_players;
extern int max_players;
extern int top_score;
extern int player_turn;
extern int quit;
extern int show_discards;
#ifdef WITH_GNUTLS
extern int no_tls;
extern char *ca_cert_file;
extern char *server_cert_file;
extern char *server_key_file;
#endif
extern unsigned char pile[4];            // Only the top card (build pile)
extern unsigned char deck[162];          // To deal
extern unsigned char deck_drawn;         // Remaining in deck
extern unsigned char deck_played[162];   // Played cards
extern unsigned char deck_played_count;  // Number of played cards

void free_players ();
void free_player (struct player_s *player);
int player_card_to_pile_from_discard (struct player_s *player, int d, int p);
int player_card_to_pile (struct player_s *player, int h, int p);
int cards_in_hand (struct player_s *player);
int player_card_to_pile_from_stock (struct player_s *player, int p);
int add_card_to_discard (struct player_s *player, int d, int h);
void draw_from_deck (struct player_s *player);
void debug_dump ();
void debug (const char *, ...);
unsigned char draw_card ();
int card_to_pile (struct player_s *player);
void discard_to_player_pile (struct player_s *player);
char *skip_digit (char *str);
char *skip_space(char *str);
char *strcat_int (char *str, int n);
char *itoa (int i);
char *card_to_str (int i);
int check_win (struct player_s *player);

#endif
