/*
    Copyright (C) 2013-2016 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdarg.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <ctype.h>
#include <signal.h>

#include "skipbo.h"
#include "server.h"

int card_to_pile (struct player_s *player);
int test_card_sequence (int p, int c);

int want_debug;
struct player_s *players;
int stock_total;
int n_players;
int max_players;
int top_score;
int player_turn;
int quit;
int show_discards;
#ifdef WITH_GNUTLS
int no_tls;
char *ca_cert_file;
char *server_cert_file;
char *server_key_file;
#endif

unsigned char pile[4];            // Only the top card (build pile)
unsigned char deck[162];          // To deal
unsigned char deck_drawn;         // Remaining in deck
unsigned char deck_played[162];   // Played cards
unsigned char deck_played_count;  // Number of played cards

void debug (const char *fmt, ...)
{
  va_list ap;
  char *buf;

  if (!want_debug)
    return;

  va_start (ap, fmt);
  vasprintf (&buf, fmt, ap);
  va_end (ap);
  fprintf (stderr, "%s", buf);
  free (buf);
}

char *itoa (int i)
{
  static char buf[16];

  snprintf (buf, sizeof(buf), "%i", i);
  return buf;
}

char *strcat_int (char *str, int n)
{
  char *buf = itoa (n);

  strcat (str, buf);
  return str+strlen (str);
}

char *skip_digit (char *str)
{
  while (isdigit (*str))
    str++;

  return str;
}

char *skip_space(char *str)
{
  while (*str == ' ')
    str++;

  return str;
}

unsigned char resolve_card (unsigned char n, unsigned char c)
{
  return c == SKIP_BO ? ++n : c;
}

void debug_dump ()
{
  int n;

  fprintf(stderr, "ERROR!\n");
  fprintf(stderr, "PILES: %i %i %i %i\n", pile[0],pile[1],pile[2],pile[3]);
  for (n = 0; n < n_players; n++)
    {
      int i;

      fprintf(stderr, "PLAYER %i: ", n+1);
      for (i = 0; i < 5; i++)
	fprintf(stderr, "%i ", players[n].hand[i]);
      fprintf(stderr, ", %i\n", players[n].stock[players[n].remaining]);
      for (i = 0; i < 4; i++)
	{
	  int x;
	  fprintf(stderr, "DISCARD %i: ", i+1);
	  for (x = 0; x < players[n].discard[i].count; x++)
	    fprintf(stderr, "%i ", players[n].discard[i].cards[x]);
	  fprintf(stderr, "\n");
	}
    }
}

unsigned char draw_card ()
{
  unsigned char c;

  if (deck_drawn + 1 == 162 || deck[deck_drawn] == NO_CARD)
    reshuffle_deck (0);

  c = deck[deck_drawn++];
  debug("DRAW from DECK %i\n", c);
  return c;
}

void draw_from_deck (struct player_s *player)
{
  int n;

  for (n = 0; n < 5; n++)
    {
      if (player->hand[n] == NO_CARD)
	{
	  unsigned char c = draw_card ();

	  player->hand[n] = c;
	}
    }
}

void free_player (struct player_s *player)
{
  free (player->name);
  free (player->buf);

#ifdef WITH_GNUTLS
  if (player->ses)
    gnutls_deinit (player->ses);
#endif

  if (player->fd != -1)
    close (player->fd);

  memset (player, 0, sizeof(struct player_s));
}

void free_players ()
{
  int i;

  for (i = 0; i < n_players; i++)
    {
      free_player (&players[i]);
    }

  free (players);
  players = NULL;
  n_players = 0;
}

int check_win (struct player_s *player)
{
  return player->remaining < 0 ? 1 : 0;

  int r = !cards_in_hand (player);
  int d;

  for (d = 0; r && d < 4; d++)
    {
      if (player->discard[d].count)
	{
	  r = 0;
	  break;
	}
    }

  return r;
}

int cards_in_hand (struct player_s *player)
{
  int n;

  for (n = 0; n < 5; n++)
    if (player->hand[n] != NO_CARD)
      return 1;

  return 0;
}

void reset_pile (int p)
{
  debug("NEW PILE\n");

  if (pile[p] != NO_CARD)
    deck_played[deck_played_count++] = pile[p];

  pile[p] = NO_CARD;
}

void add_to_pile (int p, struct player_s *player, int n)
{
  if (pile[p] != NO_CARD)
    deck_played[deck_played_count++] = pile[p];

  pile[p] = resolve_card (pile[p], player->hand[n]);
  player->hand[n] = NO_CARD;
  if (pile[p] == 12)
    reset_pile (p);
}

void add_to_pile_from_discard (int p, struct player_s *player, int n)
{
  int i = --player->discard[n].count;

  if (pile[p] != NO_CARD)
    deck_played[deck_played_count++] = pile[p];

  pile[p] = resolve_card (pile[p], player->discard[n].cards[i]);
  player->discard[n].cards[i] = NO_CARD;
  if (pile[p] == 12)
    reset_pile (p);
}

int player_card_to_pile (struct player_s *player, int h, int p)
{
  if (test_card_sequence (p, player->hand[h]))
    {
      debug("DISCARD to PILE %i, %i->%i\n",
	      p+1, pile[p], player->hand[h]);
      add_to_pile (p, player, h);
      return 0;
    }

  return 1;
}

int player_card_to_pile_from_discard (struct player_s *player, int d, int p)
{
  int n = player->discard[d].count;

  if (n--)
    {
      if (test_card_sequence (p, player->discard[d].cards[n]))
	{
	  debug("DISCARD from %i to PILE %i, %i->%i\n",
		  d+1, p+1, pile[p], player->discard[d].cards[n]);
	  add_to_pile_from_discard (p, player, d);
	  return 0;
	}
    }

  return 1;
}

int card_to_pile_from_discard (struct player_s *player)
{
  int d;

  for (d = 0; d < 4; d++)
    {
      int n = player->discard[d].count;

      if (n--)
	{
	  int p;

	  for (p = 0; p < 4; p++)
	    {
	      if (test_card_sequence (p, player->discard[d].cards[n]))
		{
		  debug("DISCARD from %i to PILE %i, %i->%i\n",
			  d+1, p+1, pile[p], player->discard[d].cards[n]);
		  add_to_pile_from_discard (p, player, d);
		  update_cards (0);
		  return card_to_pile (player);
		}
	    }
	}
    }

  return check_win (player);
}

int test_card_sequence (int p, int c)
{
  if (c == SKIP_BO || (pile[p] != NO_CARD && (pile[p] + 1 == c))
      || (pile[p] == NO_CARD && (c == 1)))
    return 1;

  return 0;
}

void add_to_pile_from_stock (int p, struct player_s *player)
{
  if (pile[p] != NO_CARD)
    deck_played[deck_played_count++] = pile[p];

  pile[p] = resolve_card (pile[p], player->stock[player->remaining--]);
  if (pile[p] == 12)
    reset_pile (p);
}

int player_card_to_pile_from_stock (struct player_s *player, int p)
{
  unsigned char c;

  if (player->remaining < 0)
    return 1;

  c = player->stock[player->remaining];

  if (test_card_sequence (p, c))
    {
      debug("DISCARD from STOCK to PILE %i, %i->%i\n",
	      p+1, pile[p], c);
      add_to_pile_from_stock (p, player);
      return 0;
    }

  return 1;
}

int card_to_pile_from_stock (struct player_s *player)
{
  int p;
  unsigned char c;

  if (player->remaining < 0)
    return 1;

  c = player->stock[player->remaining];

  for (p = 0; p < 4; p++)
    {
      if (test_card_sequence (p, c))
	{
	  debug("DISCARD from STOCK to PILE %i, %i->%i\n",
		  p+1, pile[p], c);
	  add_to_pile_from_stock (p, player);
	  update_cards (0);
	  return card_to_pile_from_stock (player);
	}
    }

  return check_win (player);
}

int card_to_pile (struct player_s *player)
{
  int h;

  if (card_to_pile_from_stock (player))
    return 1;

  for (h = 0; h < 5; h++)
    {
      int p;

      if (player->hand[h] == NO_CARD)
	continue;

      for (p = 0; p < 4; p++)
	{
	  if (test_card_sequence (p, player->hand[h]))
	    {
	      debug("DISCARD to PILE %i, %i->%i\n",
		      p+1, pile[p], player->hand[h]);
	      add_to_pile (p, player, h);
	      update_cards (0);
	      if (!cards_in_hand (player))
		draw_from_deck (player);

	      return card_to_pile (player);
	    }
	}
    }

  return card_to_pile_from_discard (player);
}

int add_card_to_discard (struct player_s *player, int d, int h)
{
  int t = player->discard[d].count;

  if (player->hand[h] == NO_CARD)
    return 0;

  if (t+1 >= MAX_DISCARD)
    {
      debug("%s(%i): %s\n",  __FILE__, __LINE__, __FUNCTION__);
      return 0;
    }

  debug("DISCARD to PLAYER pile %i, %i\n", d+1, player->hand[h]);
  player->discard[d].cards[t] = player->hand[h];
  player->hand[h] = NO_CARD;
  player->discard[d].count++;
  return 1;
}

#define CARD_IS_SEQ(a,b) (a+1 == b)
#define CARD_IS_LT(a,b) (a < b)
#define CARD_IS_LE(a,b) (a <= b)

/* FIXME heres where the hard part is. first determine the highest
 * card that cannot be played in hand to the build piles. discarding
 * those is the easiest since it doesnt consider the other players.
 */
void discard_to_player_pile (struct player_s *player)
{
  int d, i;

  for (d = 0; d < 4; d++)
    {
      int h;
      int t = player->discard[d].count;

      if (t--)
	{
	  for (h = 0; h < 5; h++)
	    {
	      {
		if (player->hand[h] != NO_CARD)
		  {
		    int a = player->hand[h];
		    int b = player->discard[d].cards[t-1];

		    if ((t >= 1 && CARD_IS_SEQ (a, b))
			|| (d+1 < 4 && player->discard[d+1].count
			    && CARD_IS_LT (a, player->discard[d].cards[t]
					   && !CARD_IS_SEQ (a, player->discard[d+1].cards[player->discard[d+1].count-1]))))
		      {
			if (!add_card_to_discard (player, d, h))
			  break;

			return;
		      }
		  }
	      }
	    }
	}
    }

  for (d = 0; d < 5; d++)
    {
      if (player->hand[d] != NO_CARD)
	break;
    }

  for (i = 0; i < 4; i++)
    if (!player->discard[i].count)
      break;

  if (i == 4)
    i = random () % 3;

  i = add_card_to_discard (player, i, d);
  if (!i)
    if (!add_card_to_discard (player, 0, d))
      if (!add_card_to_discard (player, 1, d))
	if (!add_card_to_discard (player, 2, d))
	  if (!add_card_to_discard (player, 3, d))
	    {
	      debug_dump ();
	      exit (1);
	    }

  return;
}

char *card_to_str (int i)
{
  static char buf[32];

  if (!i)
    return (char *)"-";
  else if (i == 13)
    return (char *)"SB";

  snprintf (buf, sizeof(buf), "%i", i);
  return buf;
}

void print_state (struct player_s *player)
{
  int i;
  int n = player->remaining;

  fprintf(stderr, "Build Pile: ");
  for (i = 0; i < 4; i++)
    {
      fprintf(stderr, "%s ", card_to_str (pile[i]));
    }

  fprintf(stderr, "\nHand: ");
  for (i = 0; i < 5; i++)
    {
      if (player->hand[i] == NO_CARD)
	continue;

      fprintf(stderr, "%s ", player->hand[i] == 13 ? "SB"
	      : card_to_str (player->hand[i]));
    }

  fprintf(stderr, "\nStock: %s\n", player->stock[n] == 13 ? "SB"
	  : card_to_str (player->stock[n]));

  for (i = 0; i < 4; i++)
    {
      fprintf(stderr, "Discard %i: ", i+1);

      for (n = player->discard[i].count-1; n >= 0; n--)
	{
	  fprintf(stderr, "%s ", player->discard[i].cards[n] == 13 ? "SB"
		  : card_to_str (player->discard[i].cards[n]));
	}

      if (!player->discard[i].count)
	fprintf(stderr, "%s ", card_to_str (0));

      fprintf(stderr, "\n");
    }
}

void usage (const char *pn)
{
#ifdef WITH_GNUTLS
  fprintf(stderr, "Usage: %s [-vhdDn] [-s N] [-p N] [-t N] -N | -C <file> -S <file> -K <file>\n", pn);
#else
  fprintf(stderr, "Usage: %s [-hdD] [-s <n>] [-n] | [-p N] [-t N]\n", pn);
#endif
  fprintf(stderr, "  -n      - do not fork into background\n");
  fprintf(stderr, "  -p N    - start with N AI players\n");
  fprintf(stderr, "  -s N    - number of stock cards\n");
  fprintf(stderr, "  -D N    - discard display (0-2)\n");
  fprintf(stderr, "  -t N    - scoring limit before reset\n");
  fprintf(stderr, "  -d      - enable debug output\n");
#ifdef WITH_GNUTLS
  fprintf(stderr, "  -N      - disable TLS support\n");
  fprintf(stderr, "  -C file - server CA file\n");
  fprintf(stderr, "  -S file - server certificate file\n");
  fprintf(stderr, "  -K file - key file for the server certificate\n");
#endif
  fprintf(stderr, "  -h      - this help text\n");
  fprintf(stderr, "  -v      - version information\n");
  exit (1);
}

static void catchsig (int sig)
{
  quit = 1;
}

int main (int argc, char **argv)
{
  int opt;
  char *str = NULL;
  int nofork = 0;

  show_discards = 0;
  stock_total = -1;
  max_players = 3;
  top_score = 500;
  signal (SIGINT, catchsig);
  signal (SIGTERM, catchsig);

#ifdef WITH_GNUTLS
  while ((opt = getopt (argc, argv, "D:vp:hnds:t:C:S:K:N")) != -1)
#else
  while ((opt = getopt (argc, argv, "D:vp:hnds:t:")) != -1)
#endif
    {
      switch (opt)
	{
#ifdef WITH_GNUTLS
	case 'C':
	  ca_cert_file = optarg;
	  break;
	case 'S':
	  server_cert_file = optarg;
	  break;
	case 'K':
	  server_key_file = optarg;
	  break;
        case 'N':
          no_tls = 1;
          break;
#endif
	case 'D':
	  show_discards = atoi (optarg);
	  break;
	case 't':
	  top_score = atoi (optarg);
	  break;
	case 's':
	  stock_total = atoi (optarg);
	  break;
	case 'd':
	  want_debug = 1;
	  break;
	case 'n':
	  nofork = 1;
	  break;
	case 'p':
	  max_players = strtol (optarg, &str, 10);
	  if ((str && *str) || max_players < 1 || max_players > 6)
	    usage (argv[0]);
	  break;
	case 'v':
	  fprintf(stdout, "SkipBo version %s\nCopyright (C) 2013, 2014 %s\n",
		  PACKAGE_VERSION, PACKAGE_BUGREPORT);
	  exit (0);
	default:
	  usage (argv[0]);
	}
    }

  if (stock_total != -1 && stock_total < 1)
    usage (argv[0]);
  else if (top_score < 0 || top_score > 9999)
    usage (argv[0]);
#ifdef WITH_GNUTLS
  else if (!no_tls && (!server_key_file || !server_cert_file || !ca_cert_file))
    usage (argv[0]);
#endif

  if (!nofork)
    {
      switch (fork ())
	{
        case -1:
	  fprintf(stderr, "fork(): %s\n", strerror (errno));
	  exit (1);
        case 0:
	  start_server ();
	  break;
        default:
	  break;
        }
    }
  else
    start_server ();

  exit (0);
}
