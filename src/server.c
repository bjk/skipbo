/*
    Copyright (C) 2013-2016 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <time.h>
#include <stdarg.h>

#include "skipbo.h"
#include "server.h"

#ifdef WITH_GNUTLS
static gnutls_dh_params_t dh_params;
static gnutls_certificate_credentials_t x509_cred;
#endif

enum {
    ENCRYPT_NONE, ENCRYPT_INIT, ENCRYPT_ENCRYPTED
};

enum {
  CMD_NONE, CMD_READY, CMD_ID, CMD_DEAL, CMD_GO, CMD_UPDATE, CMD_NAME,
  CMD_PLAYER_NAMES, CMD_DROP, CMD_UNREADY, CMD_CONFIG, CMD_PLAYER_ORDER
};

static int players_ready;
static int comp_players;
static int is_eog;
static int old_top_score;
static int player_order[6];
static int order_idx;
static int in_game;

static int deal_to_player (struct player_s *player);
static int check_eog ();
static void drop_player (struct player_s *, int replace);
static size_t send_config (struct player_s *player);
static char *check_for_chat (struct player_s *player, char *line, int *eof);
static int send_player_name (struct player_s *player);
static int send_player_order (struct player_s *player);
static char *fixup_cmd_id (struct player_s *player, const char *line);

#ifdef WITH_GNUTLS
static int init_tls_params ()
{
  int n;
  int bits;

  n = gnutls_certificate_allocate_credentials (&x509_cred);
  if (n != GNUTLS_E_SUCCESS)
    goto fail;

  n = gnutls_certificate_set_x509_trust_file (x509_cred, ca_cert_file,
					      GNUTLS_X509_FMT_PEM);
  if (n < 0)
    goto fail;

  n = gnutls_certificate_set_x509_key_file (x509_cred, server_cert_file,
					    server_key_file,
					    GNUTLS_X509_FMT_PEM);
  if (n != GNUTLS_E_SUCCESS)
    goto fail;

  n = gnutls_dh_params_init (&dh_params);
  if (n != GNUTLS_E_SUCCESS)
    goto fail;

  bits = gnutls_sec_param_to_pk_bits (GNUTLS_PK_DH, GNUTLS_SEC_PARAM_MEDIUM);
  n = gnutls_dh_params_generate2 (dh_params, bits);
  if (n != GNUTLS_E_SUCCESS)
    goto fail;

  gnutls_certificate_set_dh_params (x509_cred, dh_params);
  return 0;

fail:
  debug("TLS: %s\n", gnutls_strerror (n));
  return 1;
}

static int init_tls (struct player_s *player)
{
  int ret;
  const char *prio_error;

  ret = gnutls_init (&player->ses, GNUTLS_SERVER);
  if (ret != GNUTLS_E_SUCCESS)
    goto fail;

  ret = gnutls_priority_set_direct (player->ses, "SECURE256", &prio_error);
  if (ret != GNUTLS_E_SUCCESS)
    goto fail;

  ret = gnutls_credentials_set (player->ses, GNUTLS_CRD_CERTIFICATE, x509_cred);
  if (ret != GNUTLS_E_SUCCESS)
    goto fail;

  gnutls_transport_set_ptr (player->ses, (gnutls_transport_ptr_t) player->fd);
  do
    {
      ret = gnutls_handshake (player->ses);
    }
  while (ret == GNUTLS_E_INTERRUPTED || ret == GNUTLS_E_AGAIN);

  if (ret != GNUTLS_E_SUCCESS)
    goto fail;

  fcntl (player->fd, F_SETFL, O_NONBLOCK);
  player->encrypt = ENCRYPT_ENCRYPTED;
  send_player_name (player);
  return 0;

fail:
  fprintf (stderr, "TLS: %s\n", gnutls_strerror (ret));
  gnutls_deinit (player->ses);
  player->ses = NULL;
  return 1;
}
#endif

static struct command_s *lookup_cmd (struct player_s *player, int id)
{
  int n;

  if (id == -1)
    return NULL;

  for (n = 0; n < MAX_CMDS; n++)
    {
      if (player->cmds[n].id == id)
	return &player->cmds[n];
    }

  return NULL;
}

static void push_command (struct player_s *player, int cmd)
{
  int n;

  for (n = 0; n < MAX_CMDS; n++)
    {
      if (player->cmds[n].id == 0 && player->cmds[n].cmd == CMD_NONE)
	{
	  player->cmds[n].id = player->cmd.id;
	  player->cmds[n].cmd = cmd;
	  debug("PUSH %i: id=%li, cmd=%i buf='%s'\n",
		  player->id, player->cmd.id, cmd, player->result);
	  player->cmd.cmd = CMD_NONE;
	  player->cmd.id = 0;
	  return;
	}
    }

  debug("PUSH FAILED %i: id=%li, cmd=%i buf='%s'\n", player->id,
	  player->cmd.id, cmd, player->result);
}

static void update_player_pointer (struct player_s *player, int *eof)
{
  while (player->result && strchr (player->result, '\n'))
    {
      if (*player->result == '\n')
	{
	  player->result++;
	  break;
	}

      player->result++;
    }

  memset (&player->cmd, 0, sizeof(struct command_s));
  player->result = check_for_chat (player, player->result, eof);

  if (player->result && !*player->result)
    {
      free (player->buf);
      player->buf = player->result = NULL;
    }
}

/* 'update' to whether to update the player->result pointer */
static void pop_command (struct player_s *player, int update)
{
  int n;

  for (n = 0; n < MAX_CMDS; n++)
    {
      if (player->cmds[n].id && player->cmds[n].id == player->cmd.id)
	{
	  debug("POP %i: cur=%li cmd=%i buf='%s'\n", player->id,
		  player->cmds[n].id, player->cmds[n].cmd, player->result);
	  player->cmds[n].id = 0;
	  player->cmds[n].cmd = CMD_NONE;
	  memset (&player->cmd, 0, sizeof(struct command_s));
	  break;
	}
    }

  if (update)
    {
      int eof = 0;

      update_player_pointer (player, &eof);
      if (eof)
	drop_player (player, 0);
    }
}

int init_server ()
{
  return 0;
}

void reshuffle_deck (int init)
{
  int n, t;
  int q = random () % 20;

  if (!init)
    {
      debug("RESHUFFLE, played %i, drawn %i\n", deck_played_count, deck_drawn);
      if (!(deck_played_count-1) || deck_played_count > 161)
	{
	  debug_dump ();
	  exit (1);
	}

      memcpy (deck, deck_played, sizeof(deck_played));
      t = n = deck_played_count - 1;
    }
  else
    t = n = 161;

  memset (deck_played, 0, sizeof(deck_played));
  deck_played_count = 0;
  deck_drawn = 0;

  do
    {
      for (; n >= 0; n--)
	{
	  unsigned char x = deck[n];
	  unsigned char r = random () % (n ? n : t);

	  deck[n] = deck[r];
	  deck[r] = x;
	}
    } while (--q > 0);
}

void shuffle_deck ()
{
  unsigned char n;
  int i;

  for (n = 0, i = 1; n < 162; n++)
    {
      deck[n] = i++;
      if (i > SKIP_BO)
	i = 1;
    }

  reshuffle_deck (1);
}

static size_t write_to_player (struct player_s *player, const char *fmt, ...)
{
  fd_set fds;
  int n;
  va_list ap;
  char *line = NULL;
  ssize_t len;

  va_start (ap, fmt);
  if (vasprintf (&line, fmt, ap) == -1)
    {
      va_end (ap);
      return -1;
    }

  va_end (ap);

  debug("SERVER WRITE %i: '%s'", player->id, line);

  FD_ZERO (&fds);
  FD_SET (player->fd, &fds);
  n = select (player->fd + 1, NULL, &fds, NULL, NULL);
  if (n > 0)
#ifdef WITH_GNUTLS
    {
      if (player->encrypt != ENCRYPT_NONE)
	{
	  do
	    {
	      n = gnutls_record_send (player->ses, line, strlen (line));
	    }
	  while (n == GNUTLS_E_INTERRUPTED || n == GNUTLS_E_AGAIN);

	  free (line);
	  return n;
	}
      else
	{
	  len = write (player->fd, line, strlen (line));
	  free (line);
	  return len;
	}
    }
#else
  {
    len = write (player->fd, line, strlen (line));
    free (line);
    return len;
  }
#endif

  free (line);
  return -1;
}

static size_t init_player (struct player_s *player)
{
  char buf[5] = { 'I', 'D', ' ', player->id + '0', 0 };
  size_t len;

  len = write_to_player (player, "%i %s\n", player->cmd_id, buf);
  if (len > 0)
    {
      player->cmd.id = player->cmd_id++;
      push_command (player, CMD_ID);
    }

  return len;
}

static char *fixup_discards (const char *str)
{
  static char buf[LINE_MAX];
  char *b = buf;
  int last = 0;
  char *p = NULL;

  strcpy (buf, str);

  while (b && *b != 'D')
    b++;

  p = b;
  while (*b)
    {
      if (isdigit (*b))
	{
	  last = atoi (b);
	  while (isdigit (*b))
	    b++;
	}
      else if (*b == 'D' || *b == 'B')
	{
	  if (last)
	    {
	      char tmp[3];
	      char *t;

	      p += 3;
	      snprintf (tmp, sizeof(tmp), "%i", last);
	      *p++ = tmp[0];
	      if (tmp[1])
		*p++ = tmp[1];

	      *p++ = ' ';
	      t = p;
	      while (*b)
		*p++ = *b++;

	      *p = 0;
	      if (*b == 'B')
		break;

	      b = t;
	      last = 0;
	      p = b;
	    }
	  else if (*b == 'B')
	    break;
	  else
	    p = b;

	  b += 2;
	  continue;
	}
      else if (*b == 'B')
	break;

      b++;
    }

  return buf;
}

/* Updates each player to show the known top-stock and discard piles
 * of other players.
 *
 * The send line to the client should be as small in length as
 * possible. So we use single letters to represent parameters names
 * followed by parameters values.
 *
 *         N[*] = Opponent N (player ID, * = current player turn)
 *            N = Number of stock cards remaining
 *         S[C] = Top card of the 'S'tock pile for the player, 13 = SkipBo
 * D[n] [C[..]] = Player 'D'iscard pile number 1-4 followed by space
                  separated cards in the pile with the top being the last
		  in the list
 * B[n] [C[..]] = 'B'uild pile cards where N is 1-4.
 *
 * A complete line may look like this:
 *
 *     1* 6 S5 D1 2 6 7 8 D2 D3 9 9 D4 B1 11 B2 6 B3 B4 2
 *
 * Player 1 is the current turn and has 6 stock cards remaining with
 * the top stock card being '5', no discards in player discard pile D2
 * or D4 and no cards in the build pile number 3.
 */
int update_cards (int all)
{
  char buf[LINE_MAX], *b = buf;
  int p = player_turn;
  int x = all ? n_players : p + 1;

  for (p = all ? 0 : player_turn; p < x; p++)
    {
      int n;

      b = buf;
      *b++ =  players[p].id + '0'; // Player number

      if (p == player_turn)
	*b++ = '*';

      *b++ = ' ';
      *b = 0;
      b = strcat_int (buf, players[p].remaining+1);
      *b++ = ' ';
      *b++ = 'S'; // Top stock card
      *b = 0;
      n = players[p].remaining;
      b = strcat_int (buf, n >= 0 ? players[p].stock[players[p].remaining] : 0);
      *b++ = ' ';

      for (n = 0; n < 4; n++)
	{
	  int i;

	  if (show_discards == 1) // Top only
	    i = players[p].discard[n].count-1;
	  else // All
	    i = 0;

	  *b++ = 'D'; // Player discard pile
	  *b++ = n + 1 + '0';
	  *b++ = ' ';

	  for (; i < players[p].discard[n].count; i++)
	    {
	      if (players[p].discard[n].cards[i] == NO_CARD)
		continue;

	      *b = 0;
	      b = strcat_int(buf, players[p].discard[n].cards[i]);
	      *b++ = ' ';
	    }
	}

      for (n = 0; n < 4; n++)
	{
	  *b++ = 'B'; // Build pile (top) card
	  *b++ = n + 1 + '0';

	  if (pile[n] != NO_CARD)
	    {
	      *b++ = ' ';
	      *b = 0;
	      b = strcat_int (buf, pile[n]);
	    }

	  if (n + 1 != 4)
	    *b++ = ' ';
	}

      *b = 0;

      for (n = 0; n < n_players; n++)
	{
	  if (players[n].fd != -1 && players[n].ready)
	    {
	      size_t len;
	      char *tmp = buf;

	      if (show_discards == 0) // Player only
		{
		  b = buf;
		  if (*b - '0' != players[n].id)
		    tmp = fixup_discards (buf);
		}

	      debug("Sending card status to player %i\n", players[n].id);
	      len = write_to_player (&players[n], "%i %s\n", players[n].cmd_id,
				     tmp);
	      if (len > 0)
		{
		  players[n].cmd.id = players[n].cmd_id++;
		  push_command (&players[n], CMD_UPDATE);
		}
	      else
		return 1;
	    }
	}
    }

  return 0;
}

/* Read a line from the player. Four play types are known and * all
 * take two arguments with the first being the source of the card to *
 * play and the destination being where to place the card.
 *
 *    Dn Bn = P is top card from discard pile N, B is build pile N
 *       Bn = Top card from the player stock, B is build pile N
 *    Hn Bn = H is card number N from player hand, B is builds pile N
 *
 * A discard is also a line by itself and completes the turn:
 *
 *    Hn Dn = H is card number N from player hand, D is player discard pile N
 */
int parse_card (struct player_s *player, char **line)
{
  char *p;

  for (p = *line; *p; p++)
    {
      int s, d;

      if (*p == '\n')
	{
	  p++;
	  break;
	}

      if (*p == 'D') // From player pile
	{
	  s = atoi (++p);
	  p = skip_digit (p);
	  p = skip_space (p);
	  if (*p++ != 'B')
	    return 1;

	  d = atoi (p);
	  d = player_card_to_pile_from_discard (player, s-1, d-1);
	  p = skip_digit (p);
	  *line = p;
	  return d == 0 ? 2 : 1; // Keep playing
	}
      else if (*p == 'B') // From stock
	{
	  d = atoi (++p) - 1;
	  d = player_card_to_pile_from_stock(player, d);
	  p = skip_digit (p);
	  *line = p;
	  return d == 0 ? 2 : 1; // Keep playing
	}
      else if (*p == 'H') // From hand
	{
	  s = atoi (++p);
	  p = skip_digit (p);
	  p = skip_space (p);
	  if (*p != 'B') // Build pile
	    {
	      if (*p++ != 'D') // Discard
		return 1;

	      d = atoi (p);
	      p = skip_digit (p);
	      *line = p;
	      return !add_card_to_discard (player, d-1, s-1);
	    }

	  d = atoi (++p);
	  d = player_card_to_pile (player, s-1, d-1);
	  p = skip_digit (p);
	  if (!d && !cards_in_hand (player))
	    {
	      d = deal_to_player (player);
	      *line = p;
	      if (d)
		return 1;
	    }

	  *line = p;
	  return d == 0 ? 2 : d; // Keep playing
	}
      else
	return 1;
    }

  *line = p;
  return 1;
}

static char *
check_for_chat (struct player_s *player, char *line, int *eof)
{
  char *l = line, *p;
  int i;

  *eof = 0;

  if (line && *line && strchr (line, '\n'))
    {
      l = fixup_cmd_id (player, line);
      if (!l)
	{
	  *eof = 1;
	  return NULL;
	}
    }

  if (!l || (*l != 'C' || *(l+1) != ' ') || !strchr (l, '\n'))
    {
      debug("CHAT: '%s'\n", l);
      return line;
    }

  for (i = 0; i < n_players; i++)
    {
      char *buf;
      size_t len;

      if (players[i].id == player->id || players[i].fd == -1)
	continue;

#ifdef WITH_GNUTLS
      /* Prevent sending encrypted chat lines to non-encrypted clients. */
      if (player->encrypt == ENCRYPT_ENCRYPTED
	  && players[i].encrypt != ENCRYPT_ENCRYPTED)
	{
	  const char *s = "<encrypted message>\n";

	  buf = malloc (strlen (s) + 4);
	  p = buf;
	  *p++ = 'C';
	  *p++ = player->id + '0';
	  *p++ = ' ';
	  *p = 0;
	  strcat (buf, s);
	}
      else
	{
#endif
	  p = strchr (l, '\n');
	  if (p)
	    len = strlen (l)-strlen (p);
	  else
	    len = strlen (l);

	  len += 3;
	  buf = calloc (len+1, sizeof(char));
	  p = buf;
	  *p++ = 'C';
	  *p++ = player->id + '0';
	  *p++ = ' ';
	  strncat (buf, l+2, len);
	  buf[len] = 0;
#ifdef WITH_GNUTLS
	}
#endif

      if (write_to_player (&players[i], "%i %s", players[i].cmd_id, buf) > 0)
	{
	  players[i].cmd_id++; // But dont actually queue chat result
	}

      free (buf);
    }

  p = strchr (line, '\n');
  if (p && *p)
    p++;

  return check_for_chat (player, p, eof);
}

static char *fixup_cmd_id (struct player_s *player, const char *line)
{
  char *p = NULL;
  int n = strtol (line, &p, 10);

  if (!p || *p != ' ')
    {
      debug("Protocol violation: '%s'\n", line);
      return NULL;
    }

  player->cmd.id = n;

  if (player->cmd_id <= player->cmd.id)
    player->cmd_id = player->cmd.id + 1;

  return p+1;
}

/* Handles chat lines and returns a to-be freed pointer to a command result.
 * The result is stored in player->buf and 'dst' updated to the command
 * result which may have been preceeded by a chat line. */
#define BUFSIZE 2048
ssize_t read_from_player (struct player_s *player, char **dst, int *eof)
{
  char *buf = malloc (BUFSIZE), *p;
  size_t len;
#ifdef WITH_GNUTLS
  ssize_t ret;
#endif

  *eof = 0;
  memset (buf, 0, BUFSIZE);

#ifdef WITH_GNUTLS
  if (player->encrypt != ENCRYPT_NONE)
    {
    rehandshake:
      do
	{
	  ret = gnutls_record_recv (player->ses, buf, BUFSIZE);
	  if (ret == GNUTLS_E_REHANDSHAKE)
	    {
	      ret = gnutls_rehandshake (player->ses);
	      if (ret == GNUTLS_E_GOT_APPLICATION_DATA)
		goto rehandshake;
	      else if (ret != GNUTLS_E_SUCCESS)
		{
		  free (buf);
		  fprintf (stderr, "%s\n", gnutls_strerror (ret));
		  return 0;
		}

	      do
		{
		  ret = gnutls_handshake (player->ses);
		}
	      while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED);

	      continue;
	    }
	}
      while (ret == GNUTLS_E_INTERRUPTED || ret == GNUTLS_E_AGAIN);

      if (ret < 0)
	{
	  free (buf);
	  *eof = 1;
	  debug("TLS: %s\n", gnutls_strerror (ret));
	  return 0;
	}

      len = ret;
    }
  else
    len = read (player->fd, buf, BUFSIZE-1);
#else
  len = read (player->fd, buf, BUFSIZE-1);
#endif

  if (len > 0)
    buf[len] = 0;
  else
    {
      if (len)
	debug("%s: %i: %s\n", __FILE__, __LINE__, strerror (errno));
      free (buf);
      *eof = 1;
      return len;
    }

  debug("SERVER READ %i (len=%li): '%s'\n", player->id, len, buf);

  p = check_for_chat (player, buf, eof);
  if (!p || !*p)
    {
      free (buf);
      return player->buf ? strlen (player->buf) : 0;
    }

  if (player->buf)
    {
      int n = player->result ? strlen (player->buf) - strlen (player->result)
	: 0;

      len = strlen (p) + strlen (player->buf);
      player->buf = realloc (player->buf, (len+1)*sizeof(char));
      strcat (player->buf, p);
      player->result = player->buf + n;
    }
  else
    player->buf = strdup (p);

  free (buf);
  if (!*player->buf)
    {
      free (player->buf);
      player->buf = player->result = NULL;
    }

  *dst = player->buf;
  return player->buf ? strlen (player->buf) : 0;
}

void deal_player (struct player_s *player, int max)
{
  int i;

  for (i = 0; i < max; i++)
    {
      player->stock[i] = draw_card ();
      debug("PLAYER %i draw STOCK: %i\n", player->id, player->stock[i]);
    }

  player->remaining = max - 1;

  for (i = 0; i < 5; i++)
    player->hand[i] = draw_card ();
}

int init_game(int stock)
{
  int i, r;
  int s = stock != -1 ? stock : n_players > 4 ? 10 : 15;

  srandom (getpid ()+time (NULL));
  memset (pile, 0, sizeof(pile));
  memset (deck_played, 0, sizeof(deck_played));
  deck_played_count = 0;
  deck_drawn = 0;
  shuffle_deck ();

  for (r = i = 0; i < n_players; i++)
    {
      if (players[i].score >= old_top_score)
	{
	  r = 1;
	  break;
	}
    }

  for (i = 0; i < n_players; i++)
    {
      if (r)
	players[i].score = 0;

      debug("Player N=%i, ID=%i, score=%i\n", i, players[i].id,
	      players[i].score);
      deal_player (&players[i], s);
    }

  order_idx = 0;
  old_top_score = top_score;
  return 0;
}

static int lowest_comp_score (int s, int p)
{
  int i;

  for (i = 0; i < n_players; i++)
    {
      if (players[i].fd == -1)
	{
	  p = (p == -1) ? i : p;

	  if (players[i].score < s)
	    return lowest_comp_score (players[i].score, i);
	}
    }

  return p;
}

static void boot_comp_player (int for_human)
{
  int s = 0;
  int p = -1;
  int i, q;

  for (i = 0; i < n_players; i++)
    {
      if (players[i].fd == -1)
	{
	  if (players[i].score > s)
	    {
	      s = players[i].score;
	      p = i;
	    }
	}
    }

  q = lowest_comp_score (s, p);
  drop_player (&players[q], for_human);
}

struct player_s *new_player (int fd)
{
  int n = n_players;
  int i, q;

  /* Boot a comp player in favor of a human player. */
  if (n_players >= max_players && fd != -1)
    {
      boot_comp_player (1);
      return new_player (fd);
    }

  players = realloc (players, (n_players + 1) * sizeof(struct player_s));
  memset (&players[n_players++], 0, sizeof(struct player_s));
  players[n].fd = fd;

  if (fd == -1)
    {
      char buf[32];

      snprintf (buf, sizeof(buf), "Computer %i", ++comp_players);
      players[n].name = strdup (buf);
    }

  for (q = 0, i = 1; i <= 6; i++)
    {
      int x;

      for (x = 0; x < n_players; x++)
	{
	  if (players[x].id == i)
	    break;

	  if (players[x].id && players[x].id == q)
	    q = 0;
	  else if (!q)
	    q = i;
	}
    }

  players[n].cmd_id = 1;
  players[n].id = q;
  return &players[n];
}

static int deal_to_player (struct player_s *player)
{
  char buf[256], *b = buf;
  int h;

  draw_from_deck (player);
  *b++ = 'D'; // Deal/draw from deck
  *b++ = ' ';

  for (h = 0; h < 5; h++)
    {
      *b = 0;
      b = strcat_int (buf, player->hand[h]);
      if (h + 1 != 5)
	*b++ = ' ';
    }

  *b++ = 0;
  size_t len = write_to_player (player, "%i %s\n", player->cmd_id, buf);
  if (len <= 0)
    return 1;

  player->cmd.id = player->cmd_id++;
  push_command (player, CMD_DEAL);
  return 0;
}

static char *player_name (struct player_s *player)
{
  static char buf[64];
  char tmp[16];
  const char *s = "Unknown";
  char *p;

  if (!player->name)
    return NULL;

  memset (buf, 0, sizeof(buf));
  buf[0] = 0;
  p = buf;
  *p++ = 'N';

  if (player->fd == -1) // Flag computer players
    *p++ = '*';
#ifdef WITH_GNUTLS
  else if (player->encrypt == ENCRYPT_ENCRYPTED)
    *p++ = '#';
#endif

  if (player->ready || in_game)
    {
      *p++ = 'R';
      player->ready = 1;
    }

  *p++ = player->id + '0';
  *p++ = ' ';
  *p = 0;
  snprintf (tmp, sizeof(tmp), "%i", player->score);
  strncat (buf, tmp, sizeof(buf)-1);
  p = buf + strlen (buf);
  *p++ = ' ';
  strncat (buf, player->name ? player->name : s, sizeof(buf) - 2);
  return buf;
}

/* Send all client names to 'player'. */
static int send_player_names (struct player_s *player)
{
  int n;
  int ret = 0;

  for (n = 0; n < n_players; n++)
    {
      char *line = player_name (&players[n]);

      if (players[n].id == player->id)
	continue;

      if (line && write_to_player (player, "%i %s\n", player->cmd_id, line) > 0)
	{
	  player->cmd.id = player->cmd_id++;
	  push_command (player, CMD_PLAYER_NAMES);
	  ret++;
	}
    }

  return ret;
}

/* Send the 'player' name to all connected clients. */
static int send_player_name (struct player_s *player)
{
  int n;
  char *line = player_name (player);
  int ret = 0;

  for (n = 0; n < n_players; n++)
    {
      if (players[n].fd == -1 || !line)
	continue;

      if (write_to_player (&players[n], "%i %s\n", players[n].cmd_id, line) > 0)
	{
	  players[n].cmd.id = players[n].cmd_id++;
	  push_command (&players[n], CMD_NAME);
	  ret++;
	}
      else
	{
	  drop_player (&players[n], 0);
	  if (ret)
	    ret--;
	}
    }

  return ret;
}

static void player_go (struct player_s *player, int deal)
{
  if (player->fd == -1)
    return;

  update_cards (0);
  is_eog = check_eog ();
  if (is_eog)
    return;

  if (deal)
    deal_to_player (player);

  size_t len = write_to_player (player, "%i GO\n", player->cmd_id);
  if (len > 0)
    {
      player->cmd.id = player->cmd_id++;
      push_command (player, CMD_GO);
    }
}

/* Extract characters up until the first newline character. */
static char *extract_line (char *str)
{
  char *p;
  int len = 0;
  char *buf;

  for (p = str; *p && *p != '\n'; p++)
    len++;

  buf = malloc (len+1*sizeof(char));
  strncpy (buf, str, len);
  buf[len] = 0;
  return buf;
}

static int not_in_queue (struct player_s *player, int cmd)
{
  int n;

  for (n = 0; n < MAX_CMDS; n++)
    if (player->cmds[n].id != -1 && player->cmds[n].cmd == cmd)
      return 0;

  return 1;
}

static void drop_player (struct player_s *player, int replace)
{
  int n;
  char line[5] = { '!', player->id + '0', 0};
  int which = -1;

  if (replace)
    strcat (line, "R");

  debug("DROP: %i %s\n", player->id, player->name);
  for (n = 0; n < n_players; n++)
    {
      if (players[n].id == player->id || players[n].fd == -1)
	continue;

      if (write_to_player (&players[n], "%i %s\n", players[n].cmd_id, line) > 0)
	{
	  players[n].cmd.id = players[n].cmd_id++;
	  push_command (&players[n], CMD_DROP);
	}
      else
	drop_player (player, replace);
    }

  for (n = 0; n < 5; n++)
    if (player->hand[n] != NO_CARD)
      deck_played[deck_played_count++] = player->hand[n];

  for (n = 0; n < 15; n++)
    if (player->stock[n] != NO_CARD)
      deck_played[deck_played_count++] = player->stock[n];

  for (n = 0; n < 4; n++)
    {
      int i;

      for (i = 0; i < player->discard[n].count; i++)
	if (player->discard[n].cards[i] != NO_CARD)
	  deck_played[deck_played_count++] = player->discard[n].cards[i];
    }

  for (n = 0; n < n_players; n++)
    {
      if (&players[n] == player)
	{
	  which = n;
	  free_player (player);

	  while (n+1 < n_players)
	    {
	      memcpy (&players[n], &players[n+1], sizeof(struct player_s));
	      player_order[n] = player_order[n+1];
	      n++;
	    }

	  break;
	}
    }

  n_players--;

  for (n = 0; n < n_players; n++)
    {
      if (player_order[n] > which)
	player_order[n]--;
    }

  send_player_order (NULL);
}

static int check_command_result (struct player_s *player)
{
  if (player->result && !strncmp (player->result, "OK\n", 3))
    return 0;

  debug("CMD FAILED: p=%i, id=%li cmd=%i '%s'\n", player->id,
	  player->cmd.id, player->cmd.cmd, player->result);
  drop_player (player, 0);
  return 1;
}

static size_t send_config (struct player_s *player)
{
  char buf[32];
  size_t len;

  snprintf (buf, sizeof(buf), "CONFIG %i %i %i %i", max_players,
	    stock_total == -1 ? n_players <= 4 ? 15 : 10 : stock_total,
	    top_score, show_discards);
  len = write_to_player (player, "%i %s\n", player->cmd_id, buf);
  if (len > 0)
    {
      player->cmd.id = player->cmd_id++;
      push_command (player, CMD_CONFIG);
    }

  return len;
}

static void send_updated_config (struct player_s *player)
{
  int n;

  for (n = 0; n < n_players; n++)
    {
      if (players[n].fd == -1 || players[n].fd == player->fd)
	continue;

      if (send_config (&players[n]) <= 0)
	{
	  drop_player (&players[n], 0);
	  n--;
	}
    }
}

// "CONFIG nPlayers nStock tScore discards"
static int parse_cmd_config (struct player_s *player)
{
  int max, s, t, d;
  int n;
  char *p = player->result + 7;
  char *e = NULL;
  int fd = player->fd;
  int eof = 0;

  if (!player->result || strncmp ("CONFIG ", player->result, 7))
    return 0;

  max = strtol (p, &e, 10);
  if ((e && *e != ' ') || max < 2 || max > 6)
    return 0;

  p = skip_digit (p);
  p = skip_space (p);
  e = NULL;
  s = strtol (p, &e, 10);
  if ((e && *e != ' ') || s == 0 || s > 15 || s < -1)
    return 0;

  p = skip_digit (p);
  p = skip_space (p);
  e = NULL;
  t = strtol (p, &e, 10);
  if ((e && *e != ' ') || t == 0 || t > 9999 || t < -1)
    return 0;

  p = skip_digit (p);
  p = skip_space (p);
  e = NULL;
  d = strtol (p, &e, 10);
  p = skip_digit (p);
  p = skip_space (p);
  if ((e && *e && *e != '\n') || d < 0 || d > 2)
    return 0;

  max_players = max;
  stock_total = s;
  top_score = t;
  show_discards = d;

  if (write_to_player (player, "%i OK\n", player->cmd.id) <= 0)
    drop_player (player, 0);

  update_player_pointer (player, &eof);
  if (eof)
    drop_player (player, 0);

  for (n = 0; n < n_players; n++)
    {
      if (players[n].fd == -1 || players[n].fd == fd)
	continue;

      if (send_config (&players[n]) <= 0)
	{
	  drop_player (&players[n], 0);
	  n--;
	}
    }

  return 1;
}

static void undup_player_name (struct player_s *player)
{
  int n;

  for (n = 0; n < n_players; n++)
    {
      if (players[n].id == player->id || !players[n].name)
	continue;

      if (!strcmp (players[n].name, player->name))
	{
	  size_t len = strlen (player->name);

	  player->name = realloc (player->name, (len+3)*sizeof(char));
	  player->name[len++] = '_';
	  player->name[len++] = 2 + '0';
	  player->name[len++] = 0;
	  undup_player_name (player);
	  return;
	}
    }
}

// FIXME result from client?
static int player_un_ready (struct player_s *player, int un)
{
  int n;

  for (n = 0; n < n_players; n++)
    {
      char buf[] = { un ? 'U' : 'R', player->id + '0', 0 };

      if (players[n].fd == -1 || players[n].fd == player->fd)
	continue;

      (void)write_to_player (&players[n], "%i %s\n", player->cmd_id++, buf);
    }

  return 0;
}

static int send_player_order (struct player_s *player)
{
  int n;
  char buf[16], *p = buf;
  int ret = 0;

  *p++ = 'O';

  for (n = 0; n < n_players; n++)
    {
      debug("Player order: %i\n", players[player_order[n]].id);
      *p++ = ' ';
      *p++ = players[player_order[n]].id + '0';
    }

  *p = 0;

  for (n = 0; n < n_players; n++)
    {
      if (players[n].fd == -1)
	continue;

      if (write_to_player (&players[n], "%i %s\n", players[n].cmd_id, buf) <= 0)
	{
	  if (&players[n] == player)
	    ret = 1;

	  drop_player (&players[n], 0);
	}
      else
	{
	  players[n].cmd.id = players[n].cmd_id++;
	  push_command (&players[n], CMD_PLAYER_ORDER);
	}
    }

  return ret;
}

static int parse_player_line (struct player_s *player, int *eof)
{
  int err = 1;

  *eof = 0;

  do
    {
      int ret = 0;
      struct command_s *cmd = NULL;

      if (player->result)
	player->result = fixup_cmd_id (player, player->result);

      if (player->encrypt != ENCRYPT_ENCRYPTED && player->result
	  && !strncmp (player->result, "STARTTLS\n", 9))
	{
#ifdef WITH_GNUTLS
	  if (!no_tls && player->encrypt == ENCRYPT_NONE)
	    {
	      player->encrypt = ENCRYPT_INIT;
	      if (init_tls (player))
		{
		  drop_player (player, 0);
		  return 0;
		}

	      update_player_pointer (player, eof);
	      if (*eof)
		{
		  drop_player (player, 0);
		  return 0;
		}
	    }
          else
            {
#endif
          write_to_player (player, "%i ERROR TLS not implemented\n",
                           player->cmd.id);
          drop_player(player, 0);
          return 0;
#ifdef WITH_GNUTLS
            }
#endif
	}

      if (player->cmd.id == 0)
	return err;

      cmd = lookup_cmd (player, player->cmd.id);

      if (!cmd) // Client initiated command
	{
	  if (!player->result)
	    continue;

	  if (!strncmp (player->result, "READY\n", 6))
	    {
	      update_player_pointer (player, eof);
	      if (*eof)
		{
		  drop_player (player, 0);
		  return 0;
		}

	      player->ready = !player->ready ? 1 : player->ready;
	      player_un_ready (player, 0);
	      continue;
	    }
	  else if (!strncmp (player->result, "CONFIG ", 7))
	    {
	      if (!parse_cmd_config (player))
		{
		  drop_player (player, 0);
		  return 0;
		}

	      update_player_pointer (player, eof);
	      if (*eof)
		{
		  drop_player (player, 0);
		  return 0;
		}

	      continue;
	    }
	  else if (!strncmp (player->result, "UNREADY\n", 8))
	    {
	      if (write_to_player (player, "%i OK\n", player->cmd.id) <= 0)
		{
		  drop_player (player, 0);
		  return 0;
		}

	      player->ready = 0;
	      update_player_pointer (player, eof);
	      if (*eof)
		{
		  drop_player (player, 0);
		  return 0;
		}

	      player_un_ready (player, 1);
	      continue;
	    }
	  else // invalid command for state?
	    {
	      debug("Dont know how to handle: id=%li, cmd=%i, buf='%s'\n",
		      cmd ? cmd->id : -1, cmd? cmd->cmd : -1, player->result);
	      drop_player (player, 0);
	      return 0;
	    }
	}

      switch (cmd->cmd)
	{
	case CMD_PLAYER_ORDER:
	  if (check_command_result (player))
	    {
	      *eof = 1;
	      return 0;
	    }

	  pop_command (player, 1);

	  if (in_game && player->ready == 1)
	    {
	      if (deal_to_player (player))
		{
		  drop_player (player, 0);
		  return 0;
		}
	    }
	  break;
	case CMD_ID:
	  if (!player->result || !*player->result)
	    return 1;

	  if (!strncmp (player->result, "NAME ", 5))
	    {
	      free (player->name);
	      player->name = extract_line (player->result+5);
	      undup_player_name (player);
	      pop_command (player, 1);
	      if (!send_player_name (player) && !player->ready)
		;
	      else
		if (!send_player_names (player) && !player->ready)
		  ;

	      if (send_config (player) <= 0)
		{
		  drop_player (player, 0);
		  return 0;
		}

	      return 1;
	    }

	  drop_player (player, 0);
	  return 0;
	  break;
	case CMD_NAME:
	  if (!player->buf || !*player->buf)
	    return 1;

	  if (check_command_result (player))
	    {
	      *eof = 1;
	      return 0;
	    }

	  pop_command (player, 1);

	  if (!player->ready && !send_player_names (player))
	    ;
	  break;
	case CMD_PLAYER_NAMES:
	  if (check_command_result (player))
	    {
	      *eof = 1;
	      return 0;
	    }

	  pop_command (player, 1);
	  break;
	case CMD_CONFIG:
	  if (check_command_result (player))
	    {
	      *eof = 1;
	      return 0;
	    }

	  pop_command (player, 1);

	  if (!player->ready && not_in_queue (player, CMD_READY))
	    {
	      ;
	    }
	  else if (in_game)
	    {
	      if (send_player_order (player))
		return 0;
	    }
	  break;
	case CMD_DROP:
	  if (check_command_result (player))
	    {
	      *eof = 1;
	      return 0;
	    }

	  pop_command (player, 1);
	  break;
	case CMD_UNREADY:
	  if (check_command_result (player))
	    {
	      *eof = 1;
	      return 0;
	    }

	  pop_command (player, 1);

	  if (!player->ready && not_in_queue (player, CMD_READY))
	    {
	      ;
	    }
	  break;
	case CMD_UPDATE:
	  if (!player->result || !*player->result)
	    return 1;

	  if (check_command_result (player))
	    {
	      *eof = 1;
	      return 0;
	    }

	  pop_command (player, 1);
	  break;
	case CMD_DEAL:
	  if (!player->result || !*player->result)
	    return 1;

	  if (check_command_result (player))
	    {
	      *eof = 1;
	      return 0;
	    }

	  pop_command (player, 1);

	  if (player->ready == 1)
	    {
	      if (in_game)
		update_cards (1);

	      player->ready = 2;
	    }

	  break;
	case CMD_NONE:
	  debug("%s(%i): %s\n",  __FILE__, __LINE__, __FUNCTION__);
	  break;
	case CMD_GO:
	  if (!player->result || !*player->result)
	    {
	      return 1; // Waiting for read()
	    }

	  debug("PLAY %i: %i '%s' '%s'\n", player->id,
		  cmd->cmd, player->buf, player->result);
	  ret = parse_card (player, &player->result);

	  if (ret != 1) // Not an error
	    {
	      if (write_to_player (player, "%i OK\n", cmd->id) <= 0)
		{
		  err = 0;
		  break;
		}
	    }

	  if (*player->result == '\n')
	    player->result++;

	  if (ret == 1 || ret == 2) // Common
	    {
	      if (ret == 1)
		{
		  if (write_to_player (player, "%i ERROR 1 bad play\n", cmd->id) <= 0)
		    {
		      //pop_command (player, 1);
		      break;
		    }

		  pop_command (player, 1);
		  player_go (player, 0);
		  continue;
		}
	      else if (ret == 2)
		{
		  pop_command (player, 1);
		  player_go (player, 0);
		  continue;
		}
	    }
	  else // Discard to player pile
	    {
	      pop_command (player, 1);
	      update_cards (0);
	      err = 0;
	    }
	  break;
	default:
	  debug("%s(%i): %s\n",  __FILE__, __LINE__, __FUNCTION__);
	  break;
	}
    } while (player->result && *player->result && strchr (player->result, '\n'));

  return err;
}

static int adjust_player_turn (int dropped)
{
  int n;
  int t = 0;

  for (n = 0; n < n_players; n++)
    {
      if (players[n].fd != -1)
	t++;
    }

  if (!t || n_players < 2)
    {
      debug("Not enough players.\n");
      return 1;
    }

  if (dropped != -1 && dropped == player_turn-1)
    {
      player_turn = dropped;
      return 0;
    }
  else if (dropped > player_turn)
    return 0;
  else if (dropped == player_turn && player_turn + 1 < n_players)
    {
      player_go (&players[player_turn], 0);
      return 0;
    }

  do
    {
      order_idx++;
      if (order_idx >= n_players)
	order_idx = 0;
    }
  while (players[player_order[order_idx]].fd != -1
	 && !players[player_order[order_idx]].ready);

  player_turn = player_order[order_idx];
  debug("Player %s turn:\n", players[player_turn].name);
  update_cards (0);

  if (players[player_turn].fd != -1)
    {
      player_go (&players[player_turn], 1);
    }

  return 0;
}

static int wrap_parse_player_line (struct player_s *player, int *eof)
{
  int fd = player->fd;
  int dropped = -1;
  int n;

  for (n = 0; n < n_players; n++)
    {
      if (players[n].id == player->id)
	{
	  dropped = n;
	  break;
	}
    }

  n = parse_player_line (player, eof);
  is_eog = check_eog ();
  if (n)
    return n;

  if (players[player_turn].fd != fd) // disconnect from remote client
    {
      *eof = 1;

      if (adjust_player_turn (dropped)) // No more human players, restart
	return -1;
    }

  return 0;
}

static int check_eog ()
{
  if (check_win (&players[player_turn]))
    return 1;

  return 0;
}

static struct player_s *accept_player (int sockfd)
{
  struct sockaddr raddr;
  socklen_t slen = sizeof (raddr);
  int fd = accept (sockfd, (struct sockaddr *) &raddr, &slen);
  struct player_s *player;

  if (fd == -1)
    {
      if (errno != EAGAIN)
	debug("accept(): %s\n", strerror(errno));

      usleep (100000);
      return NULL;
    }

  debug("New player: fd=%i\n",fd);
  player = new_player (fd);
  if (!player)
    {
      close (fd);
      debug("Maximum configured players reached\n");
      return NULL;
    }

  init_player (player);

  if (!in_game)
    send_updated_config (player);
  else
    deal_player (player, stock_total);

  return player;
}

int start_game (int sockfd)
{
  int n;

  if (!n_players)
    {
      debug("no players have been added. cannot start.\n");
      return 1;
    }

  in_game = 1;
  memset (player_order, 0, sizeof(player_order));

  for (n = 0; n < n_players; n++)
    player_order[n] = n;

  n = n_players-1;

  #if 0
  for (; n >= 0; n--)
    {
      int x = player_order[n];
      int r = random () % (n ? n : n_players-1);

      player_order[n] = player_order[r];
      player_order[r] = x;
    }
  #endif

  init_game (stock_total);
#if 0
  player_turn = player_order[order_idx];
#endif

  player_turn = order_idx = random () % n_players;
  send_player_order (NULL);
  debug("Player %i to start\n", player_turn+1);
  update_cards (1);

  for (n = 0; n < n_players; n++)
    if (players[n].fd != -1 && n != player_turn)
      deal_to_player (&players[n]);

  if (players[player_turn].fd != -1)
    player_go (&players[player_turn], 1);

  while (!quit)
    {
      fd_set rfds, wfds;
      int i;
      struct timeval tv = {0, 70000};
      struct player_s *player = NULL;
      int eof = 0;
      int eog = 0;

      FD_ZERO (&rfds);
      FD_ZERO (&wfds);

      for (i = n = 0; i < n_players; i++)
	{
	  if (players[i].fd != -1 && players[i].result) // Pending line(s)
	    {
	      n = wrap_parse_player_line (&players[i], &eof);
	      if (n == -1)
		goto done;
	      else if (eof)
		{
		  i = -1;
		  continue;
		}
	      else if (!n)
		{
		  if (!is_eog)
		    if (adjust_player_turn (-1))
		      goto done;
		}
	    }

	  // Wait for command queue to become empty before returning
	  if (is_eog && players[i].fd != -1)
	    {
	      int x;

	      eog = 1;

	      for (x = 0; x < MAX_CMDS; x++)
		{
		  if (players[i].cmds[x].id != 0)
                    {
                      eog = 0;
                      break;
                    }
		}
	    }
	  else if (players[i].fd != -1)
	    eog = 0;
	}

      if (eog)
	break;

      for (i = n = 0; i < n_players; i++)
	{
	  if (players[i].fd != -1)
	    {
	      if (n < players[i].fd)
		n = players[i].fd;

	      FD_SET (players[i].fd, &rfds);
	    }
	}

      FD_SET (sockfd, &rfds);
      n = select (n+1, &rfds, &wfds, NULL, &tv);
      if (!n && players[player_turn].fd != -1)
	continue;
      else if (n == -1)
	{
	  debug("select(): %s\n", strerror (errno));
	  break;
	}

      if (FD_ISSET (sockfd, &rfds))
	{
	  if (n_players == 6 && comp_players)
	    boot_comp_player (1);
	  else if (max_players < 6 && max_players <= n_players)
	    max_players++;

	  if (n_players < 6)
	    {
	      struct player_s *p = accept_player (sockfd);

	      if (p)
		{
		  player_order[n_players-1] = n_players-1;
		}
	    }
	  else
	    debug("Refusing new human player due to too many players.\n");

	  continue;
	}

      /* Read a chat line or command result from a player. */
      for (i = 0; i < n_players; i++)
	{
	  if (players[i].fd == -1) // Computer player
	    continue;

	  if (FD_ISSET (players[i].fd, &rfds)) // Read ready
	    {
              ssize_t len = read_from_player (&players[i], &players[i].result,
                                              &eof);
	      if (len < 0 || eof)
		{
		  drop_player (&players[i], 0);
		  if (adjust_player_turn (i))
		    goto done;

		  i--;
		  debug("EOF from player %i, n_players=%i\n", i+1, n_players);
		  continue;
		}
	    }
	}

      player = &players[player_turn];

      if (player->fd != -1)
	{
	  if (!FD_ISSET (player->fd, &rfds))
	    continue;

	  n = wrap_parse_player_line (player, &eof);
	  if (n == 1 || is_eog)
	    continue;
	  else if (n == -1)
	    break;
	  else if (eof)
	    continue;
	}
      else // Computer players' turn
	{
	  if (is_eog)
	    continue;

	  // FIXME append card status to a buffer for each card turn
	  // to prevent blocking while writing to all clients. This
	  // way we only need to send one (comma separated) line to
	  // each client. Then let the client do what it wants (sleep)
	  // with each turn.
	  draw_from_deck (player);
	  if (!card_to_pile (player))
	    {
	      discard_to_player_pile (player);
	      update_cards (0);
	    }
	}

      is_eog = check_eog ();
      if (!is_eog)
	if (adjust_player_turn (-1))
	  goto done;
    }

done:
  return 0;
}

static void reset_players (int score)
{
  int n, x;
  int s = 0;
  int won = -1;

  for (n = x = 0; n < n_players; n++)
    {
      int eof;

      if (players[n].remaining < 0)
	{
	  s += 25;
	  won = n;
	}
      else
	s += (players[n].remaining+1) * 5;

      update_player_pointer (&players[n], &eof);
      if (eof)
	{
	  drop_player (&players[n], 0);
	  n--;
	  continue;
	}

      memset (&players[n].hand, 0, sizeof(players[n].hand));
      memset (&players[n].stock, 0, sizeof(players[n].stock));
      memset (&players[n].discard, 0, sizeof(players[n].discard));
      players[n].ready = players[n].remaining = 0;

      if (players[n].fd != -1)
	x++;

      if (score && players[n].fd != -1
	  && not_in_queue (&players[n], CMD_UNREADY))
	{
	  if (write_to_player (&players[n], "%i UNREADY\n",
			       players[n].cmd_id) > 0)
	    {
	      players[n].cmd.id = players[n].cmd_id++;
	      push_command (&players[n], CMD_UNREADY);
	    }
	  else
	    drop_player (&players[n], 0);
	}
    }

  if (score && won != -1)
    players[won].score += s;

  if (!x)
    {
      while (n_players)
	drop_player (&players[0], 0);

      comp_players = 0;
    }

  players_ready = 0;
  is_eog = 0;
  in_game = 0;
}

void start_server ()
{
  struct addrinfo hints, *servinfo, *p;
  int n;
  int sockfd = -1;
  char buf[7];

#ifdef WITH_GNUTLS
  if (!no_tls)
    {
      gnutls_global_init ();
      init_tls_params ();
    }
#endif

  memset (&hints, 0, sizeof (hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;
  snprintf (buf, sizeof (buf), "%i", 6464);

  if ((n = getaddrinfo (NULL, buf, &hints, &servinfo)) == -1)
    {
      debug("getaddrinfo(): %s", gai_strerror (n));
      return;
    }

  for (n = 0, p = servinfo; p != NULL; p = p->ai_next)
    {
      int r = 1;

      if (p->ai_family != AF_INET)
	continue;

      if ((sockfd = socket (p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
	{
	  debug("socket(): %s", strerror (errno));
	  continue;
	}

      if (setsockopt (sockfd, SOL_SOCKET, SO_REUSEADDR, &r, sizeof (int)) == -1)
	{
	  debug("setsockopt(): %s", strerror (errno));
	  freeaddrinfo (servinfo);
	  return;
	}

      if (bind (sockfd, p->ai_addr, p->ai_addrlen) == -1)
	{
	  close (sockfd);
	  debug("bind(): %s", strerror (errno));
	  continue;
	}

      n++;
      break;
    }

  freeaddrinfo (servinfo);
  if (listen (sockfd, 0) == -1)
    {
      debug("listen(): %s\n", strerror (errno));
      return;
    }

  while (!quit)
    {
      while (!quit && !players_ready)
	{
	  fd_set fds;
	  struct timeval tv = { 0, 100000 };
	  int x;
	  struct player_s *player;

	  FD_ZERO (&fds);
	  FD_SET (sockfd, &fds);

	  for (x = sockfd, n = 0; n < n_players; n++)
	    {
	      if (players[n].fd == -1)
		continue;

	      if (players[n].fd > x)
		x = players[n].fd;

	      FD_SET (players[n].fd, &fds);
	    }

	  n = select (x+1, &fds, NULL, NULL, &tv);
	  if (n == -1)
	    {
	      debug("select(): %s\n", strerror (errno));
	      free_players ();
	      return;
	    }
	  else if (!n)
	    continue;

	  if (FD_ISSET (sockfd, &fds))
	    {
	      player = accept_player (sockfd);
	      continue;
	    }

	  for (n = 0; n < n_players; n++)
	    {
	      player = &players[n];

	      if (player->fd != -1 && FD_ISSET (player->fd, &fds))
		{
		  int eof = 0;
		  size_t len = read_from_player (player, &player->result, &eof);

		  if (len > 0) // 0 = incomplete line
		    wrap_parse_player_line (player, &eof);
		  else if (len == 0 && !eof) // Chat line?
		    continue;
		  else
		    {
		      drop_player (player, 0);
		      n = -1;
		      continue;
		    }
		}
	    }

	  for (x = n = 0; n < n_players; n++)
	    {
	      player = &players[n];

	      if (player->fd != -1)
		{
		  x++;

		  if (!player->ready)
		    {
		      players_ready = 0;
		      break;
		    }

		  players_ready = 1;
		}
	    }

	  if (!x)
	      reset_players (0);
	}

      while (n_players > max_players)
	boot_comp_player (0);

      for (n = n_players+1; n <= max_players; n++)
	{
	  struct player_s *player = new_player (-1);
	  send_player_name (player);
	}

      start_game (sockfd);
      reset_players (1);
    }

  free_players ();
  n_players = 0;
}
