/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QSettings>
#include <QFileDialog>
#include <QSoundEffect>
#include <QColorDialog>
#include <QProcess>

#include "preferencesDialog.h"

PreferencesDialog::PreferencesDialog (QWidget *p) : QDialog (p)
{
  ui.setupUi (this);
  QSettings cfg ("qskipbo");
  ui.sl_gameSpeed->setSliderPosition (cfg.value ("gameSpeed", 7).toInt ());
  ui.ck_humanSpeed->setChecked (cfg.value ("humanSpeed", true).toBool ());
  ui.le_playerName->setText (cfg.value ("name", "name").toString());
  ui.le_hostname->setText (cfg.value ("hostnaame", "localhost").toString());
  ui.sp_port->setValue (cfg.value ("port", 6464).toInt ());
  ui.ck_encrypt->setChecked (cfg.value ("encryptOnConnect", false).toBool ());

  ui.ck_soundCommand->setChecked (cfg.value ("useSoundCommand", false).toBool ());
  ui.le_soundCommand->setText (cfg.value ("soundCommand", "").toString ());
  ui.pb_playTurnSound->setEnabled (false);
  ui.pb_playJoinSound->setEnabled (false);
  ui.pb_playLeaveSound->setEnabled (false);
  ui.pb_playEogSound->setEnabled (false);
  ui.pb_playEomSound->setEnabled (false);
  ui.pb_playChatSound->setEnabled (false);

  connect (ui.pb_playTurnSound, SIGNAL (clicked ()), this,
	   SLOT (slotPlayTurnSound ()));
  connect (ui.le_turnSoundFile, SIGNAL (textChanged (const QString &)),
	   this, SLOT (slotTurnSoundTextChanged (const QString &)));
  connect (ui.pb_playJoinSound, SIGNAL (clicked ()), this,
	   SLOT (slotPlayJoinSound ()));
  connect (ui.le_joinSoundFile, SIGNAL (textChanged (const QString &)),
	   this, SLOT (slotJoinSoundTextChanged (const QString &)));
  connect (ui.pb_playLeaveSound, SIGNAL (clicked ()), this,
	   SLOT (slotPlayLeaveSound ()));
  connect (ui.le_leaveSoundFile, SIGNAL (textChanged (const QString &)),
	   this, SLOT (slotLeaveSoundTextChanged (const QString &)));
  connect (ui.pb_playEogSound, SIGNAL (clicked ()), this,
	   SLOT (slotPlayEogSound ()));
  connect (ui.le_eogSoundFile, SIGNAL (textChanged (const QString &)),
	   this, SLOT (slotEogSoundTextChanged (const QString &)));
  connect (ui.pb_playEomSound, SIGNAL (clicked ()), this,
	   SLOT (slotPlayEomSound ()));
  connect (ui.le_eomSoundFile, SIGNAL (textChanged (const QString &)),
	   this, SLOT (slotEomSoundTextChanged (const QString &)));
  connect (ui.pb_playChatSound, SIGNAL (clicked ()), this,
	   SLOT (slotPlayChatSound ()));
  connect (ui.le_chatSoundFile, SIGNAL (textChanged (const QString &)),
	   this, SLOT (slotChatSoundTextChanged (const QString &)));

  ui.le_turnSoundFile->setText (cfg.value ("turnSoundFile", "").toString ());
  connect (ui.pb_turnSoundFile, SIGNAL (clicked ()), this, SLOT (slotTurnSoundFile ()));
  ui.le_joinSoundFile->setText (cfg.value ("joinSoundFile", "").toString ());
  connect (ui.pb_joinSoundFile, SIGNAL (clicked ()), this, SLOT (slotJoinSoundFile ()));
  ui.le_leaveSoundFile->setText (cfg.value ("leaveSoundFile", "").toString ());
  connect (ui.pb_leaveSoundFile, SIGNAL (clicked ()), this, SLOT (slotLeaveSoundFile ()));
  ui.le_eogSoundFile->setText (cfg.value ("eogSoundFile", "").toString ());
  connect (ui.pb_eogSoundFile, SIGNAL (clicked ()), this, SLOT (slotEogSoundFile ()));
  ui.le_eomSoundFile->setText (cfg.value ("eomSoundFile", "").toString ());
  connect (ui.pb_eomSoundFile, SIGNAL (clicked ()), this, SLOT (slotEomSoundFile ()));
  ui.le_chatSoundFile->setText (cfg.value ("chatSoundFile", "").toString ());
  connect (ui.pb_chatSoundFile, SIGNAL (clicked ()), this, SLOT (slotChatSoundFile ()));

  ui.ck_log->setChecked (cfg.value ("logChatLines", false).toBool ());
  ui.le_logFile->setText (cfg.value ("logFile", "").toString ());
  connect (ui.pb_logFile, SIGNAL (clicked ()), this, SLOT (slotChatLogFile()));
  ui.ck_timeStamp->setChecked (cfg.value ("logTimeStamp", false).toBool ());
  ui.sp_logLines->setValue (cfg.value ("appendLogLines", 100).toInt ());

  ui.le_caCert->setText (cfg.value ("caCertFile", "").toString ());
  connect (ui.pb_caCert, SIGNAL (clicked ()), this, SLOT (slotCaCert()));
  ui.le_serverCert->setText (cfg.value ("serverCertFile", "").toString ());
  connect (ui.pb_serverCert, SIGNAL (clicked ()), this, SLOT (slotServerCert()));

  serverMsgColor = cfg.value ("serverMsgColor", "Blue").value<QColor>();
  ui.pb_serverMsgColor->setStyleSheet (QString ("background-color: %1;").arg (serverMsgColor.name ()));
  otherMsgColor = cfg.value ("otherMsgColor", "Green").value<QColor>();
  ui.pb_otherMsgColor->setStyleSheet (QString ("background-color: %1;").arg (otherMsgColor.name ()));
  chatMsgColor = cfg.value ("chatMsgColor", "Black").value<QColor>();
  ui.pb_chatMsgColor->setStyleSheet (QString ("background-color: %1;").arg (chatMsgColor.name ()));
  playerMsgColor = cfg.value ("playerMsgColor", "DarkCyan").value<QColor>();
  ui.pb_playerMsgColor->setStyleSheet (QString ("background-color: %1;").arg (playerMsgColor.name ()));
  playerBgColor = cfg.value ("playerBgColor", "Grey").value<QColor>();
  ui.pb_playerBgColor->setStyleSheet (QString ("background-color: %1;").arg (playerBgColor.name ()));
  playerFgColor = cfg.value ("playerFgColor", "white").value<QColor>();
  ui.pb_playerFgColor->setStyleSheet (QString ("background-color: %1;").arg (playerFgColor.name ()));

  connect (ui.pb_serverMsgColor, SIGNAL (clicked ()), this,
	   SLOT (slotServerMsgColor()));
  connect (ui.pb_otherMsgColor, SIGNAL (clicked ()), this,
	   SLOT (slotOtherMsgColor()));
  connect (ui.pb_chatMsgColor, SIGNAL (clicked ()), this,
	   SLOT (slotChatMsgColor()));
  connect (ui.pb_playerMsgColor, SIGNAL (clicked ()), this,
	   SLOT (slotPlayerMsgColor()));
  connect (ui.pb_playerBgColor, SIGNAL (clicked ()), this,
	   SLOT (slotPlayerBgColor()));
  connect (ui.pb_playerFgColor, SIGNAL (clicked ()), this,
	   SLOT (slotPlayerFgColor()));
}

PreferencesDialog::~PreferencesDialog ()
{
  QSettings cfg ("qskipbo");
  cfg.setValue ("gameSpeed", ui.sl_gameSpeed->sliderPosition ());
  cfg.setValue ("humanSpeed", ui.ck_humanSpeed->isChecked ());
  cfg.setValue ("name", ui.le_playerName->text ());
  cfg.setValue ("hostname", ui.le_hostname->text ());
  cfg.setValue ("port", ui.sp_port->value ());
  cfg.setValue ("useSoundCommand", ui.ck_soundCommand->isChecked ());
  cfg.setValue ("soundCommand", ui.le_soundCommand->text ());
  cfg.setValue ("turnSoundFile", ui.le_turnSoundFile->text ());
  cfg.setValue ("joinSoundFile", ui.le_joinSoundFile->text ());
  cfg.setValue ("leaveSoundFile", ui.le_leaveSoundFile->text ());
  cfg.setValue ("eogSoundFile", ui.le_eogSoundFile->text ());
  cfg.setValue ("eomSoundFile", ui.le_eomSoundFile->text ());
  cfg.setValue ("chatSoundFile", ui.le_chatSoundFile->text ());
  cfg.setValue ("encryptOnConnect", ui.ck_encrypt->isChecked ());
  cfg.setValue ("logChatLines", ui.ck_log->isChecked ());
  cfg.setValue ("logFile", ui.le_logFile->text ());
  cfg.setValue ("logTimeStamp", ui.ck_timeStamp->isChecked ());
  cfg.setValue ("appendLogLines", ui.sp_logLines->value ());
  cfg.setValue ("caCertFile", ui.le_caCert->text ());
  cfg.setValue ("serverCertFile", ui.le_serverCert->text ());
  cfg.setValue ("serverMsgColor", serverMsgColor);
  cfg.setValue ("otherMsgColor", otherMsgColor);
  cfg.setValue ("chatMsgColor", chatMsgColor);
  cfg.setValue ("playerMsgColor", playerMsgColor);
  cfg.setValue ("playerBgColor", playerBgColor);
  cfg.setValue ("playerFgColor", playerFgColor);
}

void
PreferencesDialog::slotCaCert ()
{
  QFileDialog d (this, tr ("Select CA certificate file"));

  d.setNameFilter ("*.pem");

  if (d.exec ())
    ui.le_caCert->setText (d.selectedFiles ().at (0));
}

void
PreferencesDialog::slotServerCert ()
{
  QFileDialog d (this, tr ("Select server certificate file"));

  d.setNameFilter ("*.pem");

  if (d.exec ())
    ui.le_serverCert->setText (d.selectedFiles ().at (0));
}

void
PreferencesDialog::slotChatLogFile ()
{
  QFileDialog d (this, tr ("Select chat log file"));

  if (d.exec ())
    ui.le_logFile->setText (d.selectedFiles ().at (0));
}

void
PreferencesDialog::slotTurnSoundTextChanged (const QString &s)
{
  ui.pb_playTurnSound->setEnabled (!s.isEmpty ());
}

void
PreferencesDialog::slotJoinSoundTextChanged (const QString &s)
{
  ui.pb_playJoinSound->setEnabled (!s.isEmpty ());
}

void
PreferencesDialog::slotLeaveSoundTextChanged (const QString &s)
{
  ui.pb_playLeaveSound->setEnabled (!s.isEmpty ());
}

void
PreferencesDialog::slotEogSoundTextChanged (const QString &s)
{
  ui.pb_playEogSound->setEnabled (!s.isEmpty ());
}

void
PreferencesDialog::slotEomSoundTextChanged (const QString &s)
{
  ui.pb_playEomSound->setEnabled (!s.isEmpty ());
}

void
PreferencesDialog::slotChatSoundTextChanged (const QString &s)
{
  ui.pb_playChatSound->setEnabled (!s.isEmpty ());
}

void
PreferencesDialog::playSound (const QString &sound)
{
  if (sound.isEmpty ())
    return;

  if (ui.ck_soundCommand->isChecked ())
    {
      QProcess *p = new QProcess (this);
      QStringList args;

      args.append (sound);
      p->start (ui.le_soundCommand->text (), args);
      return;
    }

  QSoundEffect s;
  s.setSource (QUrl::fromLocalFile (sound));
  s.play ();
}

void
PreferencesDialog::slotPlayTurnSound ()
{
    playSound (ui.le_turnSoundFile->text ());
}

void
PreferencesDialog::slotPlayJoinSound ()
{
  playSound (ui.le_joinSoundFile->text ());
}

void
PreferencesDialog::slotPlayLeaveSound ()
{
  playSound (ui.le_leaveSoundFile->text ());
}

void
PreferencesDialog::slotPlayEogSound ()
{
  playSound (ui.le_eogSoundFile->text ());
}

void
PreferencesDialog::slotPlayEomSound ()
{
  playSound (ui.le_eomSoundFile->text ());
}

void
PreferencesDialog::slotPlayChatSound ()
{
  playSound (ui.le_chatSoundFile->text ());
}

void
PreferencesDialog::slotTurnSoundFile ()
{
  QFileDialog d (this, tr ("Select sound file"));

  d.setNameFilter ("*.wav");

  if (d.exec ())
    ui.le_turnSoundFile->setText (d.selectedFiles ().at (0));
}

void
PreferencesDialog::slotJoinSoundFile ()
{
  QFileDialog d (this, tr ("Select sound file"));

  d.setNameFilter ("*.wav");

  if (d.exec ())
    ui.le_joinSoundFile->setText (d.selectedFiles ().at (0));
}

void
PreferencesDialog::slotLeaveSoundFile ()
{
  QFileDialog d (this, tr ("Select sound file"));

  d.setNameFilter ("*.wav");

  if (d.exec ())
    ui.le_leaveSoundFile->setText (d.selectedFiles ().at (0));
}

void
PreferencesDialog::slotEogSoundFile ()
{
  QFileDialog d (this, tr ("Select sound file"));

  d.setNameFilter ("*.wav");

  if (d.exec ())
    ui.le_eogSoundFile->setText (d.selectedFiles ().at (0));
}

void
PreferencesDialog::slotEomSoundFile ()
{
  QFileDialog d (this, tr ("Select sound file"));

  d.setNameFilter ("*.wav");

  if (d.exec ())
    ui.le_eomSoundFile->setText (d.selectedFiles ().at (0));
}

void
PreferencesDialog::slotChatSoundFile ()
{
  QFileDialog d (this, tr ("Select sound file"));

  d.setNameFilter ("*.wav");

  if (d.exec ())
    ui.le_chatSoundFile->setText (d.selectedFiles ().at (0));
}

void
PreferencesDialog::slotServerMsgColor ()
{
  QColor c = QColorDialog::getColor (serverMsgColor);

  if (!c.isValid ())
    return;

  serverMsgColor.setRgba (c.rgba ());
  ui.pb_serverMsgColor->setStyleSheet (QString ("background-color: %1;").arg (serverMsgColor.name ()));
}

void
PreferencesDialog::slotOtherMsgColor ()
{
  QColor c = QColorDialog::getColor (otherMsgColor);

  if (!c.isValid ())
    return;

  otherMsgColor.setRgba (c.rgba ());
  ui.pb_otherMsgColor->setStyleSheet (QString ("background-color: %1;").arg (otherMsgColor.name ()));
}

void
PreferencesDialog::slotChatMsgColor ()
{
  QColor c = QColorDialog::getColor (chatMsgColor);

  if (!c.isValid ())
    return;

  chatMsgColor.setRgba (c.rgba ());
  ui.pb_chatMsgColor->setStyleSheet (QString ("background-color: %1;").arg (chatMsgColor.name ()));
}

void
PreferencesDialog::slotPlayerMsgColor ()
{
  QColor c = QColorDialog::getColor (playerMsgColor);

  if (!c.isValid ())
    return;

  playerMsgColor.setRgba (c.rgba ());
  ui.pb_playerMsgColor->setStyleSheet (QString ("background-color: %1;").arg (playerMsgColor.name ()));
}

void
PreferencesDialog::slotPlayerBgColor ()
{
  QColor c = QColorDialog::getColor (playerBgColor);

  if (!c.isValid ())
    return;

  playerBgColor.setRgba (c.rgba ());
  ui.pb_playerBgColor->setStyleSheet (QString ("background-color: %1;").arg (playerBgColor.name ()));
}

void
PreferencesDialog::slotPlayerFgColor ()
{
  QColor c = QColorDialog::getColor (playerFgColor);

  if (!c.isValid ())
    return;

  playerFgColor.setRgba (c.rgba ());
  ui.pb_playerFgColor->setStyleSheet (QString ("background-color: %1;").arg (playerFgColor.name ()));
}
