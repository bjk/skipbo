/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SKIPBOWIDGET
#define SKIPBOWIDGET

#include <QWidget>

#include "ui_skipboWidget.h"

class SkipBoWidget : public QWidget
{
  Q_OBJECT
 public:
  SkipBoWidget (QWidget *);
  ~SkipBoWidget ();
  void setBuildPile (int b, int card);

 private:
  friend class QSkipBo;
  Ui::SkipBoWidget ui;
};

#endif
