/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PREFERENCESDIALOG_H
#define PREFERENCESDIALOG_H

#include <QDialog>
#include <QColor>

#include "ui_preferencesDialog.h"

class PreferencesDialog : public QDialog
{
  Q_OBJECT
 public:
  PreferencesDialog (QWidget *);
  ~PreferencesDialog ();

  Ui::PreferencesDialog ui;

 private slots:
  void slotTurnSoundFile ();
  void slotJoinSoundFile ();
  void slotLeaveSoundFile ();
  void slotEogSoundFile ();
  void slotEomSoundFile ();
  void slotChatSoundFile ();
  void slotPlayTurnSound ();
  void slotPlayJoinSound ();
  void slotPlayLeaveSound ();
  void slotPlayEogSound ();
  void slotPlayEomSound ();
  void slotPlayChatSound ();
  void slotTurnSoundTextChanged (const QString &);
  void slotJoinSoundTextChanged (const QString &);
  void slotLeaveSoundTextChanged (const QString &);
  void slotEogSoundTextChanged (const QString &);
  void slotEomSoundTextChanged (const QString &);
  void slotChatSoundTextChanged (const QString &);
  void slotChatLogFile ();
  void slotCaCert ();
  void slotServerCert ();
  void slotServerMsgColor ();
  void slotOtherMsgColor ();
  void slotChatMsgColor ();
  void slotPlayerMsgColor ();
  void slotPlayerBgColor ();
  void slotPlayerFgColor ();

 private:
  void playSound (const QString &);

  QColor serverMsgColor;
  QColor otherMsgColor;
  QColor chatMsgColor;
  QColor playerMsgColor;
  QColor playerBgColor;
  QColor playerFgColor;
};

#endif
