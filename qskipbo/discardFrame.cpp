/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "discardFrame.h"

DiscardFrame::DiscardFrame (QWidget *p) : QFrame (p)
{
  ui.setupUi (this);
  Qt::WindowFlags flags = Qt::X11BypassWindowManagerHint | Qt::FramelessWindowHint;

  flags |= Qt::BypassWindowManagerHint;

  setWindowFlags (flags);
  setMaximumSize (QSize (75, 0));
  setSizePolicy (QSizePolicy::Fixed,
	       QSizePolicy::Expanding);
  setFrameStyle (QFrame::Box | QFrame::Raised);
  setStyleSheet ("background-color: DarkBlue;");
  ui.verticalLayout->setSpacing (1);
  ui.verticalLayout->setContentsMargins (1, 1, 1, 1);
}

DiscardFrame::~DiscardFrame ()
{
}

void
DiscardFrame::mousePressEvent (QMouseEvent *ev)
{
  (void)ev;
  close ();
}

void
DiscardFrame::setCards (PlayerWidget::PlayerInfo &info, int which,
			QPoint pos, bool b)
{
  int n;

  while (ui.verticalLayout->count ())
    {
      QLayoutItem *w = ui.verticalLayout->takeAt (0);

      delete w;
    }

  if (!b || !info.discard[which].count)
    {
      close ();
      return;
    }

  move (pos);

  for (n = info.discard[which].count-1; n >= 0; n--)
    {
      QSvgWidget *w = new QSvgWidget (this);
      int c = info.discard[which].cards[n];

      w->setMinimumSize (QSize (75, 105));
      w->setMaximumSize (QSize (75, 105));
      w->load (Card::cardToResourceName (c));
      ui.verticalLayout->addWidget (w);
    }

  adjustSize ();
  show ();
}
