/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef DISCARDFRAME_H
#define DISCARDFRAME_H

#include <QFrame>
#include <QMouseEvent>

#include "playerWidget.h"
#include "ui_discardFrame.h"

class DiscardFrame : public QFrame
{
 public:
  DiscardFrame (QWidget * = 0);
  ~DiscardFrame ();
  Ui::DiscardFrame ui;
  void setCards (PlayerWidget::PlayerInfo &, int, QPoint, bool = true);

 private:
  void mousePressEvent (QMouseEvent *);
};

class Card;
class ShowDiscardEvent : public QEvent
{
 public:
  ShowDiscardEvent (Card *c) : QEvent (QEvent::Type (ShowDiscardEventId))
  {
    _card = c;
  };

  Card *card ()
  {
    return _card;
  };

 private:
  Card *_card;
};

#endif
