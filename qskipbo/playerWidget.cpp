/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "playerWidget.h"
#include "discardFrame.h"
#include "card.h"

PlayerWidget::PlayerWidget (QWidget *parent) : QFrame (parent)
{
  ui.setupUi (this);
  setFrameStyle (QFrame::Box | QFrame::Raised);
  ui.f_stock->setFrameStyle (QFrame::WinPanel | QFrame::Raised);
  ui.stock->setParent (this);

  ui.discard1->setCard (0, Card::Discard);
  ui.discard1->setId (1);
  ui.discard2->setCard (0, Card::Discard);
  ui.discard2->setId (2);
  ui.discard3->setCard (0, Card::Discard);
  ui.discard3->setId (3);
  ui.discard4->setCard (0, Card::Discard);
  ui.discard4->setId (4);
  ui.discard1->setParent (this);
  ui.discard2->setParent (this);
  ui.discard3->setParent (this);
  ui.discard4->setParent (this);

  ui.hand1->setId (1);
  ui.hand2->setId (2);
  ui.hand3->setId (3);
  ui.hand4->setId (4);
  ui.hand5->setId (5);
  ui.hand1->setParent (this);
  ui.hand2->setParent (this);
  ui.hand3->setParent (this);
  ui.hand4->setParent (this);
  ui.hand5->setParent (this);

  memset (&info, 0, sizeof(info));
  _id = 0;
  _isRemovable = false;
  _score = 0;
  ui.l_score->setText ("");
  _isComputer = false;
  _remainingStock = 0;
  _isEncrypted = false;
  ui.l_encrypted->setHidden (true);
  _topScore = 0;
  _isReady = false;
  discardFrame[0] = new DiscardFrame ();
  discardFrame[1] = new DiscardFrame ();
  discardFrame[2] = new DiscardFrame ();
  discardFrame[3] = new DiscardFrame ();
  memset (&oldDiscards, 0, sizeof(struct PlayerInfo::Discards));
}

PlayerWidget::~PlayerWidget ()
{
  delete discardFrame[0];
  delete discardFrame[1];
  delete discardFrame[2];
  delete discardFrame[3];
}

void
PlayerWidget::rebuildDiscardFrame ()
{
  int n;

  for (n = 0; n < 4; n++)
    {
      if (!discardFrame[n]->isVisible ())
	continue;

      if (n == 0 && memcmp (&info.discard[n], &oldDiscards[n],
			    sizeof(struct PlayerInfo::Discards)))
	showDiscardFrame (ui.discard1);
      else if (n == 1 && memcmp (&info.discard[n], &oldDiscards[n],
			    sizeof(struct PlayerInfo::Discards)))
	showDiscardFrame (ui.discard2);
      else if (n == 2 && memcmp (&info.discard[n], &oldDiscards[n],
			    sizeof(struct PlayerInfo::Discards)))
	showDiscardFrame (ui.discard3);
      else if (n == 3 && memcmp (&info.discard[n], &oldDiscards[n],
			    sizeof(struct PlayerInfo::Discards)))
	showDiscardFrame (ui.discard4);
    }

  for (n = 0; n < 4; n++)
    memcpy (&oldDiscards[n], &info.discard[n],
	    sizeof(struct PlayerInfo::Discards));
}

void
PlayerWidget::showDiscardFrame (Card *card, bool b)
{
  int which = card->discardId ()-1;
  QPoint pos = card->mapToGlobal (card->pos ());

  discardFrame[which]->setCards (info, which, pos, b);
}

void
PlayerWidget::setName (QString s)
{
  ui.l_playerName->setText (s);
}

QString
PlayerWidget::name ()
{
  return ui.l_playerName->text ();
}

void
PlayerWidget::setStock (int n)
{
  ui.stock->setCard (n, Card::Stock);
}

void
PlayerWidget::setHand(int h, int c)
{
  if (h == 1)
    ui.hand1->setCard (c, Card::Hand);
  else if (h == 2)
    ui.hand2->setCard (c, Card::Hand);
  else if (h == 3)
    ui.hand3->setCard (c, Card::Hand);
  else if (h == 4)
    ui.hand4->setCard (c, Card::Hand);
  else if (h == 5)
    ui.hand5->setCard (c, Card::Hand);
}

void
PlayerWidget::addDiscard (int d, int c)
{
  if (d == 1)
    ui.discard1->setCard (c, Card::Discard, 1);
  else if (d == 2)
    ui.discard2->setCard (c, Card::Discard, 2);
  else if (d == 3)
    ui.discard3->setCard (c, Card::Discard, 3);
  else if (d == 4)
    ui.discard4->setCard (c, Card::Discard, 4);
}

void
PlayerWidget::setCardsEnabled (bool b)
{
  ui.hand1->setEnabled (b);
  ui.hand2->setEnabled (b);
  ui.hand3->setEnabled (b);
  ui.hand4->setEnabled (b);
  ui.hand5->setEnabled (b);
  ui.stock->setEnabled (b);
  ui.discard1->setEnabled (true);
  ui.discard2->setEnabled (true);
  ui.discard3->setEnabled (true);
  ui.discard4->setEnabled (true);
}

void
PlayerWidget::setId (int n)
{
  _id = n;
}

int
PlayerWidget::id ()
{
  return _id;
}

void
PlayerWidget::setRemovable (bool b)
{
  _isRemovable = b;
}

bool
PlayerWidget::isRemovable ()
{
  return _isRemovable;
}

void
PlayerWidget::addScore (int n)
{
  _score += n;
  ui.l_score->setText (QString (tr ("Score: %1/%2")).arg (score ()).arg (topScore ()));
}

int
PlayerWidget::score ()
{
  return _score;
}

void
PlayerWidget::resetScore ()
{
  _score = 0;
  addScore (0);
}

void
PlayerWidget::setComputer (bool b)
{
  _isComputer = b;
}

bool
PlayerWidget::isComputer ()
{
  return _isComputer;
}

void
PlayerWidget::setRemainingStock (int n)
{
  _remainingStock = n;
  ui.l_stock->setText (QString (tr ("(%1 Stock)")).arg (remainingStock ()));
}

int
PlayerWidget::remainingStock ()
{
  return _remainingStock;
}

void
PlayerWidget::setEncrypted (bool b)
{
  _isEncrypted = b;
  ui.l_encrypted->setHidden (!b);
}

bool
PlayerWidget::isEncrypted ()
{
  return _isEncrypted;
}

void
PlayerWidget::setTopScore (int n)
{
  _topScore = n;
  addScore (0); // Update display
}

int
PlayerWidget::topScore ()
{
  return _topScore;
}

void
PlayerWidget::setReady (bool b)
{
  _isReady = b;
}

bool
PlayerWidget::isReady ()
{
  return _isReady;
}

bool
PlayerWidget::event (QEvent *ev)
{
  if (ev->type () == ShowDiscardEventId)
    {
      ShowDiscardEvent *tev = static_cast<ShowDiscardEvent *>(ev);
      Card *c = tev->card ();

      showDiscardFrame (c);
      ev->accept ();
      return true;
    }

  return QWidget::event (ev);
}
