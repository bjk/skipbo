PREFIX = /usr/local
BINDIR = /usr/local/bin
LIBDIR = /usr/local/lib

QT += gui
QT += widgets
QT += network
QT += svgwidgets
QT += multimedia

CONFIG += release x86 x86_64
#CONFIG += ppc ppc64

HEADERS += $$PWD/playerWidget.h \
           $$PWD/card.h \
           $$PWD/skipboWidget.h \
           $$PWD/qskipbo.h \
           $$PWD/preferencesDialog.h \
           $$PWD/events.h \
           $$PWD/discardFrame.h

SOURCES += $$PWD/playerWidget.cpp \
           $$PWD/card.cpp \
           $$PWD/main.cpp \
           $$PWD/skipboWidget.cpp \
           $$PWD/qskipbo.cpp \
           $$PWD/preferencesDialog.cpp \
           $$PWD/discardFrame.cpp

FORMS += $$PWD/playerWidget.ui \
         $$PWD/qskipbo.ui \
         $$PWD/skipboWidget.ui \
         $$PWD/preferencesDialog.ui \
         $$PWD/discardFrame.ui

RESOURCES += qskipbo.qrc

contains (WITH_CLANG, 1) {
    include($$PWD/clang.conf)
}

contains(ANDROID_TARGET_ARCH,x86_64) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}
