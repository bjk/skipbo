/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QApplication>
#include <QMouseEvent>
#include <iostream>
#include <QDrag>
#include <QMimeData>

#include "card.h"
#include "playerWidget.h"
#include "discardFrame.h"

Card::Card (QWidget *parent) : QSvgWidget (parent)
{
  setCard (0, None);
  _type = 0;
  _card = 0;
  _id = 0;
  _discardId = 0;
  _destCard = NULL;
  _parent = NULL;

  //setAlignment (Qt::AlignHCenter|Qt::AlignVCenter);
  //setFrameStyle (QFrame::NoFrame);
  setMinimumSize (QSize (75, 105));
  setMaximumSize (QSize (75, 105));
  setSizePolicy (QSizePolicy::Fixed, QSizePolicy::Fixed);
}

Card::~Card ()
{
}

void
Card::setParent (PlayerWidget *w)
{
  if (type () == BuildPile)
    return;

  _parent = w;
}

QString
Card::cardToResourceName (int n)
{
  if (n == 13)
    return ":sb.svg";

  return QString (":/%1.svg").arg (n);
}

void
Card::setCard (int n, int t, int d)
{
  load (cardToResourceName (n));
  _card = n;
  _type = t;
  _discardId = d;

  if (t == Stock || t == Hand)
    setAcceptDrops (false);
}

int
Card::card ()
{
  return _card;
}

int
Card::type ()
{
  return _type;
}

void
Card::mouseReleaseEvent (QMouseEvent * ev)
{
  QWidget::mouseReleaseEvent (ev);
}

void
Card::mousePressEvent (QMouseEvent * ev)
{
  if ((ev->buttons () & Qt::RightButton) && type () == Discard)
    {
      ShowDiscardEvent *tev = new ShowDiscardEvent (this);
      QCoreApplication::postEvent (parentWidget (), tev);

      return;
    }

  dragStartPosition = ev->pos ();
  QWidget::mousePressEvent (ev);
}

bool
Card::finalizeDnD (Card *src, Card *dst, QString &cmd)
{
  if (dst->type () == Hand || dst->type () == Stock
      || (dst->type () == Discard && src->type () == Stock)
      || (dst->type () == Discard && src->type () == Discard))
    return false;

  if (src->type () == Hand)
    cmd.append (QString ("H%1").arg (src->id ()));
  else if (src->type () == Discard)
    cmd.append (QString ("D%1").arg (src->id ()));

  if (dst->type () == BuildPile && src->type () == Stock)
    cmd.append (QString ("B%1").arg (dst->id ()));
  else if (dst->type () == BuildPile)
    cmd.append (QString ("B%1").arg (dst->id ()));
  else if (dst->type () == Discard)
    {
      cmd.append (QString ("D%1").arg (dst->id ()));
    }

  src->setDestCard (dst);
  return true;
}

void
Card::mouseMoveEvent (QMouseEvent * ev)
{
  if ((ev->pos () - dragStartPosition).manhattanLength ()
      < QApplication::startDragDistance ())
    return;

  QDrag *drag = new QDrag (this);
  QMimeData *m = new QMimeData;
  drag->setMimeData (m);
  drag->setPixmap (QPixmap (cardToResourceName (card ())));

  if ((ev->buttons () & Qt::LeftButton))
    {
      Qt::DropAction action = drag->exec (Qt::MoveAction);

      if (drag->target ())
	{
	  Card *src = static_cast<Card *>(drag->source ());
	  Card *dst = static_cast<Card *>(drag->target ());
	  QString cmd = QString();
	  bool b = finalizeDnD (src, dst, cmd);

	  if (!b)
	    return;

	  CardCommandEvent *cev = new CardCommandEvent (cmd);
	  cev->setSourceCard (src);
	  QCoreApplication::postEvent (this->window (), cev);
	}
    }
}

void
Card::setId (int n)
{
  _id = n;
}

int
Card::id ()
{
  return _id;
}

int
Card::discardId ()
{
  return _discardId;
}

void
Card::dragMoveEvent (QDragMoveEvent * ev)
{
  QWidget::dragMoveEvent (ev);
}

void
Card::enterEvent (QEvent * ev)
{
  (void) ev;
}

void
Card::leaveEvent (QEvent * ev)
{
  (void) ev;
}

bool
Card::validateDnDMove (QDragEnterEvent *ev)
{
  Card *src = static_cast<Card *>(ev->source ());

  return src->type () != BuildPile;
}

void
Card::dragEnterEvent (QDragEnterEvent * ev)
{
  if (validateDnDMove (ev))
    {
      ev->acceptProposedAction ();
    }
}

PlayerWidget *
Card::parentWidget ()
{
  return _parent;
}

void
Card::setDestCard (Card *c)
{
  _destCard = c;
}

Card *
Card::destCard ()
{
  return _destCard;
}
