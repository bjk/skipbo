/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H

#include <QWidget>
#include <QVBoxLayout>

#include "ui_playerWidget.h"

#define MAX_DISCARD 15

class Card;
class DiscardFrame;
class PlayerWidget : public QFrame
{
  Q_OBJECT
 public:
  PlayerWidget (QWidget *);
  ~PlayerWidget ();
  void setName (QString);
  QString name ();
  void setStock (int);
  void setHand (int hand, int card); // Hand card 1 is 1
  void addDiscard (int d, int card); // Discard 1 is 1
  void setCardsEnabled (bool = true);
  void setId (int);
  int id ();
  void setRemovable (bool = true);
  bool isRemovable ();
  void addScore (int);
  int score ();
  void resetScore ();
  void setComputer (bool = true);
  bool isComputer ();
  void setEncrypted (bool = true);
  bool isEncrypted ();
  void setRemainingStock (int);
  int remainingStock ();
  void setTopScore (int);
  int topScore ();
  void setReady (bool = true);
  bool isReady ();
  void showDiscardFrame (Card *, bool = true);
  void rebuildDiscardFrame ();

  struct PlayerInfo
  {
    int hand[5];
    int stock;
    struct Discards {
      int cards[MAX_DISCARD]; // FIXME limit???
      int count;
    } discard[4];
  } info;

  Ui::PlayerWidget ui;

 private:
  bool event (QEvent *);

  struct PlayerInfo::Discards oldDiscards[4];
  int _id;
  bool _isRemovable;
  int _score;
  bool _isComputer;
  int _remainingStock;
  bool _isEncrypted;
  int _topScore;
  bool _isReady;
  DiscardFrame *discardFrame[4];
};

#endif
