/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QSKIPBO_H
#define QSKIPBO_H

#include <QMainWindow>
#include <QSslSocket>
#include <QObject>
#include <QTimer>
#include <QStringList>
#include <QMutex>

#include "playerWidget.h"
#include "ui_qskipbo.h"

#define QSKIPBO_VERSION "0.1.20"

class QSkipBo : public QMainWindow
{
  Q_OBJECT
 public:
  QSkipBo ();
  ~QSkipBo ();
  bool setHost (QString host, int port = 6464);

 private slots:
  void slotStateChanged (QAbstractSocket::SocketState);
  void slotSocketError (QAbstractSocket::SocketError);
  void slotServerLine (QString);
  void slotReady ();
  void slotUnReady ();
  void slotHostNameChanged (const QString &);
  void slotPlayerNameChanged (const QString &);
  void slotConnect ();
  void slotParsePendingLines ();
  void slotChatLine ();
  void slotReadReady ();
  void slotQuit ();
  void slotPreferences ();
  void setDefaults ();
  void slotUpdatePressed ();
  void slotAbout (bool);
  void slotDisconnect (bool);
  void slotEncrypt (bool);
  void slotSslError (const QSslError &);
  void slotEncrypted ();

 private:
  enum
  {
    CHAT_SERVER, CHAT_USER, CHAT_LOCAL
  };

  qint64 writeToServer (const QString &);
  bool parsePlayer (QString &line);
  bool playTurn (QString);
  PlayerWidget *widgetForPlayer (int, bool alloc = false);
  void updateCards (int);
  void parsePlayerName (QChar *);
  bool event (QEvent *);
  void eog (int);
  void updateCurrentPlayer ();
  bool parseServerLine (QString &);
  void appendChatLine (int which, const QString &, bool self = false,
		       bool doLog = true);
  void updatePlayerWidgets ();
  void removePlayer (PlayerWidget *, bool withWidget = true);
  void playerReady (PlayerWidget *, bool = true);
  void playSound (const QString &);
  void closeEvent (QCloseEvent *);
  void sortPlayers ();
  void rearrangePlayers (const QString &order);

  QSslSocket *socket;
  Ui::QSkipBo ui;
  Card *lastCard;
  QTimer *waitTimer;
  QMutex pendingMutex;
  QStringList pendingLines;
};

class ErrorEvent : public QEvent
{
 public:
  ErrorEvent(QString e) : QEvent (QEvent::Type (ErrorEventId))
  {
    _error = e;
  };

  QString error ()
  {
    return _error;
  };

 private:
  QString _error;
};

class EogEvent : public QEvent
{
 public:
  EogEvent(int n) : QEvent (QEvent::Type (EogEventId))
  {
    _player = n;
  };

  int player ()
  {
    return _player;
  };

 private:
  int _player;
};
#endif
