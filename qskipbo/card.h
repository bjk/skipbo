/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CARD_H
#define CARD_H

#include <QSvgWidget>
#include <QEvent>

#include "events.h"

class PlayerWidget;
class Card : public QSvgWidget
{
  Q_OBJECT
 public:
  Card (QWidget *);
  ~Card ();

  void setCard (int, int, int d = 0);
  int card ();
  int type ();
  void setId (int);
  int id ();
  int discardId ();
  void setDestCard (Card *);
  Card *destCard ();
  void setParent (PlayerWidget *);
  PlayerWidget *parentWidget ();
  static QString cardToResourceName (int n);

  enum
  {
    None, Stock, BuildPile, Discard, Hand
  };

 private:
  void mousePressEvent (QMouseEvent *);
  void mouseReleaseEvent (QMouseEvent *);
  void mouseMoveEvent (QMouseEvent *);
  void dragEnterEvent (QDragEnterEvent *);
  void dragMoveEvent (QDragMoveEvent *);
  void enterEvent (QEvent *);
  void leaveEvent (QEvent *);
  bool validateDnDMove (QDragEnterEvent *);
  bool finalizeDnD (Card *, Card *, QString &);

  QPoint dragStartPosition;
  int _card;
  int _type;
  int _id;
  int _discardId;
  Card *_destCard;
  PlayerWidget *_parent;
  Card *showingDiscard;
};

class CardCommandEvent : public QEvent
{
 public:
  CardCommandEvent (QString cmd) : QEvent (QEvent::Type (CardCommandEventId))
  {
    _command = cmd;
  };

  QString command ()
  {
    return _command;
  };

  void setSourceCard (Card *c)
  {
    _card = c;
  };

  void clearSourceCard ()
  {
    _card = NULL;
  };

  Card *sourceCard ()
  {
    return _card;
  };

 private:
  QString _command;
  Card *_card;
};

#endif
