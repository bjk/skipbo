/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <QMessageBox>
#include <QTimer>
#include <QSvgRenderer>
#include <QSettings>
#include <QSoundEffect>
#include <QFile>
#include <QDateTime>
#include <QProcess>
#include <QSslConfiguration>

#include "qskipbo.h"
#include "skipboWidget.h"
#include "card.h"
#include "playerWidget.h"
#include "preferencesDialog.h"

static QList<PlayerWidget *> playerWidgets;
static int buildPile[4];
static int currentPlayer;
static int self;
static bool reDeal;
static int gameSpeed;
static QString turnSound;
static QString joinSound;
static QString leaveSound;
static QString eogSound;
static QString eomSound;
static QString chatSound;
static bool humanSpeed;
static bool isEog;
static bool goReady;
static bool encryptOnConnect;
static bool logChat;
static QString logFile;
static QString caCertFile;
static QString serverCertFile;
static QColor serverMsgColor;
static QColor otherMsgColor;
static QColor chatMsgColor;
static QColor playerMsgColor;
static QColor playerBgColor;
static QColor playerFgColor;
static bool newGame;
static int oldTopScore;
static bool lastWasError;
static bool logTimeStamp;
static int appendLogLines;
static int cmdId;
static int currentCmdId;
static int goCmdId;

static QChar *skipSpace (QChar *str)
{
  while (*str == ' ')
    str++;

  return str;
}

static QChar *skipDigit (QChar *str)
{
  while ((*str).isDigit ())
    str++;

  return str;
}


QSkipBo::QSkipBo ()
{
  ui.setupUi (this);

  socket = NULL;
  newGame = true;
  cmdId = 1;
  connect (ui.centralwidget->ui.le_playerName,
	   SIGNAL (textChanged (const QString &)), this,
	   SLOT (slotPlayerNameChanged (const QString &)));
  connect (ui.centralwidget->ui.le_hostName,
	   SIGNAL (textChanged (const QString &)), this,
	   SLOT (slotHostNameChanged (const QString &)));
  connect (ui.centralwidget->ui.pb_connect, SIGNAL (clicked ()), this,
	   SLOT (slotConnect ()));
  ui.centralwidget->ui.pb_connect->setEnabled (false);

  connect (ui.centralwidget->ui.pb_ready, SIGNAL (clicked ()), this, SLOT (slotReady ()));
  ui.centralwidget->ui.pb_ready->setHidden (true);
  ui.centralwidget->ui.pb_update->setHidden (true);
  ui.centralwidget->ui.f_config->setHidden (true);
  lastCard = NULL;
  waitTimer = new QTimer (this);
  waitTimer->setSingleShot (true);
  connect (waitTimer, SIGNAL (timeout ()), this, SLOT (slotParsePendingLines ()));
  ui.centralwidget->ui.f_buildPile->setHidden (true);
  ui.centralwidget->ui.f_players1->setHidden (true);
  ui.centralwidget->ui.f_players2->setHidden (true);

  connect (ui.centralwidget->ui.le_chat, SIGNAL (returnPressed ()), this,
	   SLOT (slotChatLine ()));
  ui.centralwidget->ui.te_chat->setOpenLinks (1);
  ui.centralwidget->ui.te_chat->setOpenExternalLinks (1);

  ui.m_file->addAction (tr ("Preferences"), QKeySequence ("CTRL+P"), this,
                        SLOT (slotPreferences ()));
  ui.m_file->addAction (tr ("Quit"), QKeySequence ("CTRL+Q"), this,
                        SLOT (slotQuit()));

  QAction *a = new QAction (tr ("About"), this);
  connect (a, SIGNAL (triggered (bool)), this, SLOT (slotAbout (bool)));
  ui.menubar->addAction (a);

  connect (ui.centralwidget->ui.pb_update, SIGNAL (clicked ()), this,
	   SLOT (slotUpdatePressed ()));

  connect (ui.a_disconnect, SIGNAL (triggered (bool)), this,
	   SLOT (slotDisconnect (bool)));
  ui.a_disconnect->setEnabled (false);
  connect (ui.a_encrypt, SIGNAL (triggered (bool)), this,
	   SLOT (slotEncrypt (bool)));
  ui.a_encrypt->setEnabled (false);

  setDefaults ();
  ui.statusbar->showMessage (tr ("Not connected."));

  if (logChat && appendLogLines)
    {
      QFile f (logFile);

      if (f.open (QIODevice::ReadOnly | QIODevice::Text))
	{
	  QStringList list;
	  int n;

	  do
	    {
	      QString s = f.readLine ();

	      if (!s.isEmpty ())
		{
		  s.chop (2); // \r\n
		  list.append (s);
		}
	    }
	  while (!f.error () && !f.atEnd ());

	  n = list.count () < appendLogLines ? 0 : list.count () - appendLogLines;
	  for (; n < list.count (); n++)
	    appendChatLine (CHAT_LOCAL, list.at (n)+"<br />", false, false);
	}
    }

#ifdef Q_OS_ANDROID
  ui.menubar->setNativeMenuBar (false);
  showMaximized ();
#endif
}

QSkipBo::~QSkipBo ()
{
}

void
QSkipBo::closeEvent (QCloseEvent *ev)
{
  (void)ev;
}

void
QSkipBo::slotDisconnect (bool b)
{
  (void)b;
  socket->abort ();
}

void
QSkipBo::slotEncrypted ()
{
  ui.a_encrypt->setEnabled (false);
  if (encryptOnConnect)
    {
      PlayerWidget *w = widgetForPlayer (self);
      (void)writeToServer (QString ("%1 NAME %2\n").arg (currentCmdId).arg (w->name ()));
    }
}

void
QSkipBo::slotEncrypt (bool b)
{
  (void)b;
  writeToServer (QString ("%1 STARTTLS\n").arg (cmdId++));
  PlayerWidget *w = widgetForPlayer (self);
  w->setEncrypted ();
  appendChatLine (CHAT_SERVER, QString (tr ("Your connection is now encrypted")));
  socket->startClientEncryption ();
}

void
QSkipBo::slotAbout (bool b)
{
  (void)b;

  QMessageBox m (QMessageBox::Information, tr ("About QSkip-Bo"),
		 QString (tr (
			  "QSkip-Bo version %1.<br />"
			  "<br />"
			  "Copyright (C) 2013, 2014 Ben Kibbey &lt;<a href=\"mailto:bjk@luxsci.net\">bjk@luxsci.net</a>&gt;<br />"
			  "<br />"
			  "Released under the terms of the GNU General Public License version 2 (GPLv2)."
			      )).arg (QSKIPBO_VERSION),
		 QMessageBox::Ok, this);
  m.setTextFormat (Qt::RichText);
  m.exec ();
}

void
QSkipBo::setDefaults ()
{
  QSettings cfg ("qskipbo");
  gameSpeed = cfg.value ("gameSpeed", 7).toInt ();
  humanSpeed = cfg.value ("humanSpeed", true).toBool ();
  ui.centralwidget->ui.sp_port->setValue (cfg.value ("port", 6464).toInt ());
  ui.centralwidget->ui.le_playerName->setText (cfg.value ("name", "name").toString ());
  ui.centralwidget->ui.le_hostName->setText (cfg.value ("hostname", "localhost").toString ());
  turnSound = cfg.value ("turnSoundFile", "").toString ();
  joinSound = cfg.value ("joinSoundFile", "").toString ();
  leaveSound = cfg.value ("leaveSoundFile", "").toString ();
  eogSound = cfg.value ("eogSoundFile", "").toString ();
  eomSound = cfg.value ("eomSoundFile", "").toString ();
  chatSound = cfg.value ("chatSoundFile", "").toString ();
  encryptOnConnect = cfg.value ("encryptOnConnect", false).toBool ();
  logChat = cfg.value ("logChatLines", false).toBool ();
  logFile = cfg.value ("logFile", "").toString ();
  logTimeStamp = cfg.value ("logTimeStamp", false).toBool ();
  appendLogLines = cfg.value ("appendLogLines", 100).toInt ();
  caCertFile = cfg.value ("caCertFile", "").toString ();
  serverCertFile = cfg.value ("serverCertFile", "").toString ();
  serverMsgColor = cfg.value ("serverMsgColor", "Blue").value<QColor>();
  otherMsgColor = cfg.value ("otherMsgColor", "Green").value<QColor>();
  chatMsgColor = cfg.value ("chatMsgColor", "Black").value<QColor>();
  playerMsgColor = cfg.value ("playerMsgColor", "DarkCyan").value<QColor>();
  playerBgColor = cfg.value ("playerBgColor", "Gray").value<QColor>();
  playerFgColor = cfg.value ("playerFgColor", "white").value<QColor>();
  updateCurrentPlayer ();
}

void
QSkipBo::slotUpdatePressed ()
{
  QString s = "CONFIG ";
  QList<PlayerWidget *>::iterator it;

  for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
    (*it)->setTopScore (ui.centralwidget->ui.sp_score->value ());

  s.append (QString ("%1 ").arg (ui.centralwidget->ui.sp_players->value ()));
  s.append (QString ("%1 ").arg (ui.centralwidget->ui.sp_stock->value ()));
  s.append (QString ("%1 ").arg (ui.centralwidget->ui.sp_score->value ()));
  s.append (QString ("%1").arg (ui.centralwidget->ui.cb_discard->currentIndex ()));
  writeToServer (QString ("%1 %2\n").arg (cmdId++).arg (s));
}

void
QSkipBo::playSound (const QString &sound)
{
  QSettings cfg ("qskipbo");

  if (sound.isEmpty ())
    return;

  if (cfg.value ("useSoundCommand", false).toBool ())
    {
      QProcess *p = new QProcess (this);
      QStringList args;

      args.append (sound);
      p->start (cfg.value ("soundCommand", "").toString (), args);
      return;
    }

  QSoundEffect s;
  s.setSource (QUrl::fromLocalFile (sound));
  s.play();
}

void
QSkipBo::appendChatLine (int which, const QString &str, bool isSelf, bool doLog)
{
  int n = self;
  int x = 0;
  QDateTime t = QDateTime::currentDateTime ();

  if (doLog && !isSelf && str.at (1).isDigit ())
    {
      n = str.at (1).toLatin1 () - '0';
      x = 3;
    }

  if (n < 0 || n >= 6)
    return;

  if (doLog && n != self)
    playSound (chatSound);

  PlayerWidget *w = widgetForPlayer (n);
  QString s = QString();

  if (doLog && logTimeStamp)
      s = "<font color=\"" + otherMsgColor.name () + "\">" + t.toString ("[MM.dd.yyyy hh:mm:ss] ") + "</font>";

  switch (which)
    {
    case CHAT_USER:
      s += "<font color=\"" + playerMsgColor.name () + "\">" + w->name () + ":</font> " + "<font color=\"" +chatMsgColor.name () + "\">" + str.mid (x) + "</font>";
      break;
    case CHAT_SERVER:
      s += "<font color=\"" + serverMsgColor.name () + "\">" + str.mid (x) + "</font>";
      break;
    case CHAT_LOCAL:
      s += "<font color=\"" + otherMsgColor.name () + "\">" + str.mid (x) + "</font>";
      break;
    }

  QTextCursor c = ui.centralwidget->ui.te_chat->textCursor ();
  c.movePosition (QTextCursor::End);
  c.insertHtml (s);
  c.insertHtml ("<br />");
  c.movePosition (QTextCursor::End);
  ui.centralwidget->ui.te_chat->setTextCursor (c);
  ui.centralwidget->ui.le_chat->setFocus ();

  if (doLog && logChat && !logFile.isEmpty ())
    {
      QFile f (logFile);

      if (f.open (QIODevice::Append | QIODevice::Text))
	{
	  if (!logTimeStamp)
	    s.prepend (t.toString ("[MM.dd.yyyy hh:mm:ss] "));

	  s.append ("\n");
	  f.write (s.toUtf8 ());
	  f.close ();
	}
    }
}

void
QSkipBo::slotQuit ()
{
  QApplication::quit ();
}

void
QSkipBo::slotPreferences ()
{
  PreferencesDialog *w = new PreferencesDialog (this);
  bool b = false;

  if (w->exec ())
    {
      b = true;
    }

  delete w;
  if (b)
    setDefaults ();
}

void
QSkipBo::slotChatLine ()
{
  QString s = ui.centralwidget->ui.le_chat->text ();

  if (s.at (s.length ()-1) == '\n')
    s.chop (1);

  if (s.isEmpty ())
    return;

  if (writeToServer (QString ("%1 C %2\n").arg (cmdId++).arg (s)) > 0)
    {
      appendChatLine (CHAT_USER, ui.centralwidget->ui.le_chat->text (), true);
    }

  ui.centralwidget->ui.le_chat->clear ();
}

void
QSkipBo::slotPlayerNameChanged (const QString &str)
{
  ui.centralwidget->ui.pb_connect->setEnabled (!str.isEmpty ()
					       && !ui.centralwidget->ui.le_hostName->text ().isEmpty ());
}

void
QSkipBo::slotHostNameChanged (const QString &str)
{
  ui.centralwidget->ui.pb_connect->setEnabled (!str.isEmpty ()
					       && !ui.centralwidget->ui.le_playerName->text ().isEmpty ());
}

void
QSkipBo::slotConnect ()
{
  setHost (ui.centralwidget->ui.le_hostName->text (),
	   ui.centralwidget->ui.sp_port->value ());
}

static bool endOfGameOrMatch ()
{
  QList<PlayerWidget *>::iterator it;

  for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
    {
      if ((*it)->score () >= oldTopScore)
	return true;
    }

  return false;
}

bool
QSkipBo::event (QEvent *ev)
{
  int type = ev->type ();

  if (type == CardCommandEventId && currentPlayer == self)
    {
      CardCommandEvent *cev = static_cast<CardCommandEvent *>(ev);
      Card *src = cev->sourceCard ();
      Card *dst = src->destCard ();

      if (src->parentWidget () != widgetForPlayer (self)
	  || (dst->parentWidget ()
	      && dst->parentWidget () != widgetForPlayer (self)))
	{
	  ev->accept ();
	  return true;
	}

      if (goReady && playTurn (QString ("%1 %2\n").arg (goCmdId).arg (cev->command ())))
	{
	  lastCard = cev->sourceCard ();
	}

      goCmdId = 0;
      cev->clearSourceCard ();
      ev->accept ();
      return true;
    }
  else if (type == ErrorEventId)
    {
      ErrorEvent *e = static_cast<ErrorEvent *>(ev);
      QMessageBox m (QMessageBox::NoIcon, tr ("Error from server"),
		     e->error ());
      m.exec ();
      ev->accept ();
      return true;
    }
  else if (type == EogEventId)
    {
      bool b;

      disconnect (ui.centralwidget->ui.pb_ready, SIGNAL (clicked ()), this,
	       SLOT (slotUnReady ()));
      connect (ui.centralwidget->ui.pb_ready, SIGNAL (clicked ()), this,
	       SLOT (slotReady ()));
      ui.centralwidget->ui.pb_ready->setText (tr ("Ready"));
      EogEvent *e = static_cast<EogEvent *>(ev);
      PlayerWidget *w = widgetForPlayer (e->player ());

      b = endOfGameOrMatch ();
      if (b)
	{
	  playSound (eogSound);
	  appendChatLine (CHAT_LOCAL, QString (tr ("Player %1 wins the game")).arg (w->name ()));
	  QMessageBox m (QMessageBox::NoIcon, tr ("Game Over"),
			 QString (tr ("%1 wins!")).arg (w->name ()));
	  m.exec ();
	}
      else
	{
	  playSound (eomSound);
	  appendChatLine (CHAT_LOCAL, QString (tr ("Player %1 wins the match")).arg (w->name ()));
	}
    }

  return QWidget::event (ev);
}

void
QSkipBo::slotReady ()
{
  PlayerWidget *w = widgetForPlayer (self);
  size_t ready = 0, comp = 0;
  QList<PlayerWidget *>::iterator it;

  if (!w->isReady ())
    {
      if (writeToServer (QString ("%1 READY\n").arg (cmdId++)) <= 0)
	{
	}

      w->setReady ();

      for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
	{
	  if ((*it)->isComputer ())
	    comp++;

	  if ((*it)->isReady ())
	    ready++;
	}

      if (newGame)
	{
	  if (endOfGameOrMatch ()) // Game
	    {
	      for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
		(*it)->resetScore ();
	    }
	}
    }

  ui.centralwidget->ui.f_buildPile->setHidden (false);
  appendChatLine (CHAT_SERVER, ready == playerWidgets.size () - comp
		  ? QString (tr ("Starting game"))
		  : QString (tr ("Waiting for other players")));

  disconnect (ui.centralwidget->ui.pb_ready, SIGNAL (clicked ()), this,
	   SLOT (slotReady ()));
  connect (ui.centralwidget->ui.pb_ready, SIGNAL (clicked ()), this,
	   SLOT (slotUnReady ()));
  ui.centralwidget->ui.pb_ready->setText (tr ("Click to cancel Ready"));

  // Joining an already running game.
  if (w->isReady ())
    {
      #if 0
      ui.centralwidget->ui.f_config->setHidden (true);
      ui.centralwidget->ui.pb_ready->setHidden (true);
      ui.centralwidget->ui.pb_update->setHidden (true);
      #endif
#ifndef Q_OS_ANDROID
      adjustSize ();
#endif
    }
}

void
QSkipBo::slotUnReady ()
{
  if (socket->state () == QAbstractSocket::UnconnectedState
      || writeToServer (QString ("%1 UNREADY\n").arg (cmdId++)) > 0)
    {
      PlayerWidget *w = widgetForPlayer (self);

      if (w)
	w->setReady (false);

      disconnect (ui.centralwidget->ui.pb_ready, SIGNAL (clicked ()), this,
	       SLOT (slotUnReady ()));
      connect (ui.centralwidget->ui.pb_ready, SIGNAL (clicked ()), this,
	       SLOT (slotReady ()));
      ui.centralwidget->ui.pb_ready->setText (tr ("Ready"));
      appendChatLine (CHAT_LOCAL, QString (tr ("You are no longer ready")));
    }
}

bool
QSkipBo::setHost (QString host, int port)
{
  if (socket)
    delete socket;

  socket = new QSslSocket (this);
  socket->setSocketOption (QAbstractSocket::KeepAliveOption, 1);
  connect (socket, SIGNAL (stateChanged(QAbstractSocket::SocketState)), this,
	   SLOT (slotStateChanged(QAbstractSocket::SocketState)));
  connect (socket, SIGNAL (errorOccurred (QAbstractSocket::SocketError)), this,
	   SLOT (slotSocketError (QAbstractSocket::SocketError)));
  std::cout << "Connecting to host " << host.toUtf8 ().data () << " via port "
	    << port << "\n";
  connect (socket, SIGNAL (readyRead ()), this, SLOT (slotReadReady ()));
  connect (socket, SIGNAL (peerVerifyError (const QSslError &)), this,
	   SLOT (slotSslError (const QSslError &)));
  connect (socket, SIGNAL (encrypted ()), this, SLOT (slotEncrypted ()));

  QSslConfiguration conf = socket->sslConfiguration ();
  conf.addCaCertificates (caCertFile);
  socket->setSslConfiguration (conf);
  QList <QSslCertificate> list = QSslCertificate::fromPath (serverCertFile);
  if (!list.isEmpty ())
    {
      QSslError e = QSslError (QSslError::HostNameMismatch, list.at (0));
      QList <QSslError> ignore;
      ignore.append (e);
      socket->ignoreSslErrors (ignore);
    }

  socket->connectToHost (host, port);
  return true;
}

void
QSkipBo::slotSslError (const QSslError &e)
{
  std::cout << "SSL ERROR: " << e.error () << ": " << e.errorString().toUtf8().data() << "\n";
}

void
QSkipBo::slotReadReady ()
{
  do
    {
      QString s = socket->readLine ();

      if (!s.isEmpty ())
	slotServerLine (s);
    } while (socket->canReadLine ());
}

void
QSkipBo::slotStateChanged (QAbstractSocket::SocketState s)
{
  QList<PlayerWidget *>::iterator it;

  std::cout << "Socket state: " << s << "\n";

  switch (s)
    {
    case QAbstractSocket::HostLookupState:
      ui.statusbar->showMessage (tr ("Looking up host ..."));
      ui.a_disconnect->setEnabled (true);
      ui.a_encrypt->setEnabled (true);
      break;
    case QAbstractSocket::ConnectingState:
      ui.statusbar->showMessage (tr ("Connecting ..."));
      break;
    case QAbstractSocket::ConnectedState:
      ui.statusbar->showMessage (tr ("Connected."));
      ui.centralwidget->ui.f_connect->setHidden (true);
      ui.centralwidget->ui.f_players1->setHidden (false);
      ui.centralwidget->ui.pb_ready->setHidden (false);
      ui.centralwidget->ui.pb_ready->setEnabled (false);
      ui.centralwidget->ui.pb_update->setHidden (false);
      ui.centralwidget->ui.pb_update->setEnabled (false);
      ui.centralwidget->ui.f_config->setHidden (false);
      ui.centralwidget->ui.le_chat->setFocus ();
      ui.statusbar->setHidden (true);
      appendChatLine (CHAT_SERVER, QString (tr ("Connected to server")));
      break;
    case QAbstractSocket::UnconnectedState:
      for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
	removePlayer (*it);

      self = 0;
      slotUnReady ();
      ui.statusbar->setHidden (false);
      ui.statusbar->showMessage (tr ("Not connected."));
      ui.centralwidget->ui.f_connect->setHidden (false);
      ui.centralwidget->ui.f_buildPile->setHidden (true);
      ui.centralwidget->ui.f_players1->setHidden (true);
      ui.centralwidget->ui.f_players2->setHidden (true);
      ui.centralwidget->ui.pb_ready->setHidden (true);
      ui.centralwidget->ui.pb_update->setHidden (true);
      ui.centralwidget->ui.f_config->setHidden (true);
      ui.a_disconnect->setEnabled (false);
      ui.a_encrypt->setEnabled (false);
      break;
    default:
      break;
    }

#ifndef Q_OS_ANDROID
  adjustSize ();
#endif
}

void
QSkipBo::slotSocketError (QAbstractSocket::SocketError e)
{
  QMessageBox m (QMessageBox::Critical, tr ("Socket error"),
		 QString ("%1: %2").arg (ui.centralwidget->ui.le_hostName->text ()).arg (socket->errorString ()));
  m.exec ();
  std::cout << "Socket error " << e << ": "
	    << socket->errorString ().toUtf8 ().data () << "\n";
}

qint64
QSkipBo::writeToServer (const QString &line)
{
  qint64 len = socket->write (line.toUtf8 ());

  if (len == -1)
    {
      socket->abort ();
      appendChatLine (CHAT_SERVER, QString (tr ("Disconnected from server")));
      return -1;
    }

  socket->flush ();
  std::cout << "WROTE: '" << line.toUtf8 ().data () << "'";
  return len;
}

template <class T>
struct compare_players
{
  bool operator() (const T *a, const T *b) const
  {
    PlayerWidget wa = *a;
    PlayerWidget wb = *b;

    return wa.id () > wb.id ();
  }
};

void
QSkipBo::sortPlayers ()
{
#if 0
  int n, i = ui.centralwidget->ui.playersLayout->count ();
  QLayoutItem *l = ui.centralwidget->ui.playersLayout->takeAt (i?i-1:0);

  for (n = 1; n < playerWidgets.size (); n++)
    {
      PlayerWidget *w = widgetForPlayer (n);

      ui.centralwidget->ui.playersLayout->removeWidget (w);
    }

  //qsort (playerWidgets, playerWidgets.size (), sizeof(PlayerWidget), compare_players);
  std::sort (playerWidgets.begin (), playerWidgets.end (),
	     compare_players<PlayerWidget>());

  for (n = 1; n < playerWidgets.size (); n++)
    {
      PlayerWidget *w = widgetForPlayer (n);

      ui.centralwidget->ui.playersLayout->addWidget (w);
    }

  ui.centralwidget->ui.playersLayout->addItem (l);
#endif
#ifndef Q_OS_ANDROID
  adjustSize ();
#endif
}

void
QSkipBo::removePlayer (PlayerWidget *w, bool withWidget)
{
  QList<PlayerWidget *>::iterator it;

  // Is this the self widget (sorting)?
  if (ui.centralwidget->ui.playersLayout->indexOf (w) == -1)
    return;

  for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
    {
      if (*it == w)
	{
          playerWidgets.erase (it);
          break;
	}
    }

  int n = ui.centralwidget->ui.playersLayout->count ();
  QLayoutItem *l = ui.centralwidget->ui.playersLayout->takeAt (n?n-1:0);
  ui.centralwidget->ui.playersLayout->removeWidget (w);
  ui.centralwidget->ui.playersLayout->addItem (l);

  if (withWidget)
    delete w;

  sortPlayers ();
}

PlayerWidget *
QSkipBo::widgetForPlayer (int player, bool alloc)
{
  PlayerWidget *w = NULL;
  QList<PlayerWidget *>::iterator it;

  for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
    {
      if (*it && (*it)->id () == player)
	{
          w = *it;
	  break;
	}
    }

  if (w && !w->isRemovable ())
    {
      w->setHidden (false);
#ifndef Q_OS_ANDROID
      adjustSize ();
#endif
      return w;
    }

  if (w) // Other player widgets left over from previous game
    {
      if (isEog) // When in game, remove the widget.
	return NULL;

      removePlayer (w);
      return widgetForPlayer (player, alloc);
    }

#ifndef Q_OS_ANDROID
  adjustSize ();
#endif

  if (!alloc)
    return NULL;

  w = new PlayerWidget (this);
  if (player == self)
    {
      QLayoutItem *l = ui.centralwidget->ui.selfLayout->takeAt (0);
      ui.centralwidget->ui.selfLayout->addWidget (w);
      ui.centralwidget->ui.selfLayout->addItem (l);
    }
  else
    {
      int n = ui.centralwidget->ui.playersLayout->count ();
      QLayoutItem *l = ui.centralwidget->ui.playersLayout->takeAt (n?n-1:0);
      ui.centralwidget->ui.playersLayout->addWidget (w);
      ui.centralwidget->ui.playersLayout->addItem (l);
      w->ui.f_hand->setHidden (true);
    }

  w->setTopScore (ui.centralwidget->ui.sp_score->value ());
  w->setId (player);
  playerWidgets.push_back (w);
  sortPlayers ();
  return w;
}

void
QSkipBo::updateCurrentPlayer ()
{
  QList<PlayerWidget *>::iterator it;

  for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
    {
      PlayerWidget *w = widgetForPlayer ((*it)->id ());

      if (!w || w->name ().isEmpty ())
	continue;

      if (w->id () == currentPlayer)
	w->setStyleSheet (QString ("background-color: %1; color: %2").arg (playerBgColor.name ()).arg (playerFgColor.name ()));
      else
	w->setStyleSheet (QString());
    }
}

void
QSkipBo::updateCards (int player)
{
  PlayerWidget *w = widgetForPlayer (player);
  int i;

  if (player != self)
    {
      w->ui.f_hand->setHidden (true);
    }

  w->setStock (w->info.stock);
  for (i = 0; i < 5; i++)
    w->setHand (i+1, w->info.hand[i]);

  for (i = 0; i < 4; i++)
    {
      int n = w->info.discard[i].count;

      if (n--)
	w->addDiscard (i+1, w->info.discard[i].cards[n]);
      else
	w->addDiscard (i+1, Card::None);
    }

  w->rebuildDiscardFrame ();
  ui.centralwidget->setBuildPile (1, buildPile[0]);
  ui.centralwidget->setBuildPile (2, buildPile[1]);
  ui.centralwidget->setBuildPile (3, buildPile[2]);
  ui.centralwidget->setBuildPile (4, buildPile[3]);
}

static QChar *
asciiToInt (QChar *str, int *dst)
{
  QChar *p = str;
  QString s = QString();

  while ((*p).isDigit ())
    s.append (*p++);

  *dst = s.toInt ();
  return p;
}

bool
QSkipBo::parsePlayer (QString &line)
{
  QChar *data = line.data (), *p;
  int player = 0;
  PlayerWidget *w = NULL;

  memset (buildPile, 0, sizeof(buildPile));

  for (p = data; *p != 0; p++)
    {
      int n;

      if ((*p).isDigit ())
	{
	  player = (*p).toLatin1 () - '0';
	  p = skipDigit (p);
	  if (*p == '*')
	    {
	      currentPlayer = player;
	      updateCurrentPlayer ();
	      p++;
	    }

	  w = widgetForPlayer (player);

	  for (n = 0; w && n < 4; n++)
	    {
	      memset (w->info.discard[n].cards, 0,
                      sizeof (w->info.discard[n].cards));
	      w->info.discard[n].count = 0;
	    }

	  if (w)
	    {
	      int n;

	      if (*p == 0)
		return false;

	      p = asciiToInt (++p, &n);
	      w->setRemainingStock (n);
	    }
	}
      else if (*p == 'S')
	{
	  if (w)
	    {
	      if (*p == 0)
		return false;

	      p = asciiToInt (++p, &w->info.stock);
	      if (!w->info.stock) // EOG
		eog (w->id ());
	    }
	}
      else if (*p == 'D')
	{
	  if (*p == 0)
	    return false;

	  n = (*++p).toLatin1 () - '0' - 1;
	  p = skipDigit (p);

	  while (w && *p == ' ' && (*(p+1)).isDigit ())
	    {
	      p = skipSpace (p);
	      p = asciiToInt (p, &w->info.discard[n].cards[w->info.discard[n].count++]);
	    }
	}
      else if (*p == 'B')
	{
	  if (*p == 0)
	    return false;

	  int t = (*++p).toLatin1 () - '0' - 1;

	  p = skipDigit (p);
	  if (*p == ' ' && (*(p+1)).isDigit ())
	    {
	      p = skipSpace (p);
	      p = asciiToInt (p, &buildPile[t]);
	      p = skipDigit (p);
	    }

	  if (*p == 0)
	    break;
	}
      else
	{
	  std::cout << "PLAYER PARSER ERROR: '" << QString (p).toUtf8 ().data () << "'\n";
	  return false;
	}
    }

  if (w)
    updateCards (w->id ());

  return true;
}

static void
parseIntArray(int dest[], int *len, QChar *line)
{
  QChar *p;
  int s = 0;

  line = skipSpace (line);
  for (p = line; *p != 0 && *p != '\n'; p++)
    {
      p = asciiToInt (p, &dest[s++]);

      if (*p != 0 && *p != ' ' && *p != '\n')
	{
	  std::cout << "parse error: " << p << "\n";
	  return;
	}

      if (*p == 0 || *p == '\n')
	break;
    }

  *len = s;
}

bool
QSkipBo::playTurn (QString cmd)
{
  goReady = false;
  return writeToServer (cmd) > 0 ? true : false;
}

void
QSkipBo::playerReady (PlayerWidget *w, bool b)
{
  w->setReady (b);
  appendChatLine (CHAT_LOCAL, b
		  ? QString (tr ("Player %1 is ready")).arg (w->name ())
		  : QString (tr ("Player %1 is no longer ready")).arg (w->name ()));
}

void
QSkipBo::parsePlayerName (QChar *line)
{
  QChar *p = line+1;
  int n;
  PlayerWidget *w;
  bool isComp = *p == '*' ? true : false;
  bool isEncrypted = *p == '#' ? true : false;
  bool exists = true;
  bool e = false;
  bool isReady = false;
  int s = 0;

  if (isComp || isEncrypted)
    p++;

  if (*p == 'R')
    {
      isReady = true;
      p++;
    }

  n = (*p).toLatin1 () - '0';
  w = widgetForPlayer (n);

  if (!w)
    {
      exists = false;
      w = widgetForPlayer (n, true);
    }

  e = w->isEncrypted ();
  w->setComputer (isComp);
  w->setEncrypted (isEncrypted);
  p = skipDigit (p);
  p = skipSpace (p);
  w->resetScore ();
  p = asciiToInt (p, &s);
  w->addScore (s);
  p = skipDigit (p);
  p = skipSpace (p);
  w->setName (QString (p));
  ui.centralwidget->ui.f_players2->setHidden (false);

  if (n != self && !exists)
    {
      appendChatLine (CHAT_SERVER, QString (tr ("Player %1 has joined the game")).arg (w->name ()));

      if (!w->isComputer ())
	playSound (joinSound);
    }

  if (exists && isEncrypted && !e)
    {
      appendChatLine (CHAT_SERVER, QString (tr ("%1 connection is now encrypted")).arg (n == self ? tr ("Your") : w->name () + "'s"));
    }

  if (isReady && !w->isReady ())
    playerReady (w);

  // Joining an existing game.
  if (isReady && n == self)
    slotReady ();
}

void
QSkipBo::eog (int n)
{
  QList<PlayerWidget *>::iterator it;
  int s = 25;
  PlayerWidget *w = NULL;

  if (isEog)
    return;

  for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
    {
      PlayerWidget *w = *it;

      if (!w || w->id () == n || w->isRemovable ())
	continue;

      w->showDiscardFrame (w->ui.discard1, false);
      w->showDiscardFrame (w->ui.discard2, false);
      w->showDiscardFrame (w->ui.discard3, false);
      w->showDiscardFrame (w->ui.discard4, false);

      s += w->remainingStock () * 5;
    }

  w = widgetForPlayer (n);
  w->addScore (s);
  EogEvent *ev = new EogEvent(n);
  QCoreApplication::postEvent (this->window (), ev);
  isEog = true;
  newGame = true;
}

void
QSkipBo::slotParsePendingLines ()
{
  pendingMutex.lock ();
  while (socket->isValid () && pendingLines.count ())
    {
      QString s = QString (pendingLines.first ());
      pendingLines.removeFirst ();

      if (s.at (s.length ()-1) == '\n')
	s.chop (1);

      QChar *p = s.data ();
      p = asciiToInt (p, &currentCmdId);
      if (cmdId <= currentCmdId)
	cmdId = currentCmdId+1;

      s = QString (p+1);
      pendingMutex.unlock ();
      bool b = parseServerLine (s);
      PlayerWidget *w = widgetForPlayer (currentPlayer);

      if (b && s.at (0).isDigit () && s.at (1) == '*')
	{
	  if ((currentPlayer != self &&
	       (w->isComputer () || (!w->isComputer () && !humanSpeed))))
	    {
	      waitTimer->start ((10-gameSpeed)*200);
	      return;
	    }
	}

      pendingMutex.lock ();
    }

  pendingMutex.unlock ();
}

void
QSkipBo::slotServerLine (QString line)
{
  pendingMutex.lock ();
  std::cout << "READ: '" << line.toUtf8 ().data () << "'\n";
  QChar *p = line.data ();

  if ((*p).isDigit ())
    {
      p = asciiToInt (p, &currentCmdId);
      if (cmdId <= currentCmdId)
	cmdId = currentCmdId+1;

      if (*p++ == ' ' && *p++ == 'C' && (*p).isDigit ())
	{
	  p--;
	  appendChatLine (CHAT_USER, QString (p));
	  pendingMutex.unlock ();
	  return;
	}
      else
	pendingLines.append (line);
    }

  pendingMutex.unlock ();

  if (waitTimer->isActive ())
    return;

  waitTimer->start (1);
}

void
QSkipBo::updatePlayerWidgets ()
{
  QList<PlayerWidget *>::iterator it;

  for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
    {
      PlayerWidget *w = widgetForPlayer ((*it)->id ());

      if (!w)
	continue;

      bool b = w->id () == self && self == currentPlayer;
      w->setCardsEnabled (b);
    }
}

void
QSkipBo::rearrangePlayers (const QString &order)
{
  QList<PlayerWidget *>::iterator it;
  QStringList olist = order.split (" ");
  int n = 0;
  QList<PlayerWidget *> newList;

  while (n < olist.count ())
    {
      for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
	{
	  if ((*it)->id () == olist.at (n).toInt ())
	    {
	      newList.append (*it);
	      removePlayer (*it, false);
	      break;
	    }
	}

      n++;
    }

  for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
    {
      if ((*it)->isRemovable ())
	delete *it;
    }

  playerWidgets = newList;
  QLayoutItem *l = ui.centralwidget->ui.playersLayout->takeAt (0);

  for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
    {
      if ((*it)->id () == self)
	continue;

      ui.centralwidget->ui.playersLayout->addWidget (*it);
    }

  ui.centralwidget->ui.playersLayout->addItem (l);
}

bool
QSkipBo::parseServerLine (QString &line)
{
  qint64 len;
  PlayerWidget *w = NULL;
  static int lastPlayer = 0;

  if (line.at (0) == 'I') // ID
    {
      self = line.at (3).toLatin1 () - '0';
      w = widgetForPlayer (self, true);
      w->setName (ui.centralwidget->ui.le_playerName->text ());

      if (encryptOnConnect)
	slotEncrypt (false);
      else
	len = writeToServer (QString ("%1 NAME %2\n").arg (currentCmdId).arg (w->name ()));
    }
  else if (line.at (0) == 'R') // Player N ready
    {
      PlayerWidget *w = widgetForPlayer (line.at (1).toLatin1 () - '0');
      if (w)
	playerReady (w);
    }
  else if (line == "UNREADY")
    {
      isEog = false;
      if (writeToServer (QString ("%1 OK\n").arg (currentCmdId)) > 0)
	{
	  ui.centralwidget->ui.pb_ready->setHidden (false);
	  ui.centralwidget->ui.pb_update->setHidden (false);
	  ui.centralwidget->ui.f_config->setHidden (false);
	}
      PlayerWidget *w = widgetForPlayer (self);
      w->setReady (false);
    }
  else if (line.at (0) == 'U') // Player N not ready
    {
      PlayerWidget *w = widgetForPlayer (line.at (1).toLatin1 () - '0');
      if (w)
	playerReady (w, false);
    }
  else if (line.at (0) == 'N') // Player names
    {
      parsePlayerName (line.data ());
      len = writeToServer (QString ("%1 OK\n").arg (currentCmdId));
    }
  else if (line == "OK")
    {
      if (lastCard)
	{
	  w = widgetForPlayer (currentPlayer);
	  if (w)
	    {
	      lastCard->setCard (0, lastCard->type ());

	      if (lastCard->type () == Card::Hand && !reDeal)
		w->info.hand[lastCard->id ()-1] = 0;
	      else if (lastCard->type () == Card::BuildPile && !reDeal)
		buildPile[lastCard->id ()-1] = 0;
	      else if (lastCard->type () == Card::Discard)
		w->info.discard[lastCard->discardId ()-1].count--;

	      reDeal = false;
	    }
	}

      lastCard = NULL;
    }
  else if (line.at (0) == 'O') // Player order
    {
      writeToServer (QString ("%1 OK\n").arg (currentCmdId));
      rearrangePlayers (line.mid (2));
    }
  else if (line.at (0) == 'E') // ERROR
    {
      ErrorEvent *ev = new ErrorEvent(line.mid (6));
      QCoreApplication::postEvent (this->window (), ev);
      lastCard = NULL;
      lastWasError = true;
    }
  else if (line.at (0) == 'D')
    {
      int n;

      w = widgetForPlayer (self);
      if (w)
	{
	  memset (w->info.hand, 0, sizeof(w->info.hand));
	  parseIntArray (w->info.hand, &n, line.data ()+1);
	  len = writeToServer (QString ("%1 OK\n").arg (currentCmdId));
	  if (len > 0)
	    updateCards (self);

	  if (lastCard)
	    reDeal = true;
	}
    }
  else if (line.at (0).isDigit ())
    {
      if (newGame)
	{
          QList<PlayerWidget *>::iterator it;

	  newGame = false;
	  oldTopScore = ui.centralwidget->ui.sp_score->value ();

          for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
	    (*it)->setTopScore (oldTopScore);
	}

      ui.centralwidget->ui.f_config->setHidden (true);
      ui.centralwidget->ui.pb_ready->setHidden (true);
      ui.centralwidget->ui.pb_update->setHidden (true);

      if (parsePlayer (line))
	{
	  len = writeToServer (QString ("%1 OK\n").arg (currentCmdId));
	  w = widgetForPlayer (currentPlayer);
	  if (w)
	    {
	      if (lastPlayer != currentPlayer)
		{
		  appendChatLine (CHAT_LOCAL, QString (tr ("%1 turn")).arg (currentPlayer == self ? tr ("Your") : w->name () + "'s"));
		  lastPlayer = currentPlayer;
		}

	      return true;
	    }
	}
    }
  else if (line == "GO")
    {
      goReady = true;
      updatePlayerWidgets ();
      if (!lastWasError)
	playSound (turnSound);

      lastWasError = false;
      goCmdId = currentCmdId;
    }
  else if (line.left (7) == "CONFIG ")
    {
      QChar *p = line.data ();
      int n;
      QList<PlayerWidget *>::iterator it;

      p = asciiToInt (p+7, &n);
      ui.centralwidget->ui.sp_players->setValue (n);
      p = asciiToInt (++p, &n);
      ui.centralwidget->ui.sp_stock->setValue (n);
      p = asciiToInt (++p, &n);
      ui.centralwidget->ui.sp_score->setValue (n);
      p = asciiToInt (++p, &n);
      ui.centralwidget->ui.cb_discard->setCurrentIndex (n);

      for (it = playerWidgets.begin (); it != playerWidgets.end (); ++it)
	(*it)->setTopScore (ui.centralwidget->ui.sp_score->value ());

      writeToServer (QString ("%1 OK\n").arg (currentCmdId));

      if (self)
	{
	  ui.centralwidget->ui.pb_update->setEnabled (true);
	  ui.centralwidget->ui.pb_ready->setEnabled (true);
	}
    }
  else if (line.at (0) == 'C') // Chat line
    {
      if (line.at (1).isDigit () && line.at (2) == ' ')
	{
	  appendChatLine (CHAT_USER, line);
	}
    }
  else if (line.at (0) == '!') // Player dropped
    {
      PlayerWidget *w = widgetForPlayer (line.at (1).toLatin1 () - '0');

      if (w)
	{
	  appendChatLine (CHAT_SERVER, QString (tr ("Player %1 has left the game")).arg (w->name ()));
	  w->setRemovable ();
	  if (!w->isComputer () || line.at (2).toLatin1 () == 'R') // Replace existing player
	    w = widgetForPlayer (line.at (1).toLatin1 () - '0'); // Remove
	}

      len = writeToServer (QString ("%1 OK\n").arg (currentCmdId));
      playSound (leaveSound);
    }
  else if (line.at (0) != 0)
    {
      socket->abort ();
      std::cerr << "parse error from server: " << line.toUtf8 ().data ();
    }

  return false;
}
