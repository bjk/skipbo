/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef EVENTS_H
#define EVENTS_H

#define CardCommandEventId QEvent::User+1
#define ErrorEventId       QEvent::User+2
#define EogEventId         QEvent::User+3
#define ShowDiscardEventId QEvent::User+4

#endif
