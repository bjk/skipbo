/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QApplication>
#include <QStringList>
#include <iostream>

#include "qskipbo.h"

static void
usage ()
{
  std::cout << "Usage: qskipbo\n";
  exit (0);
}

int
main (int argc, char **argv)
{
  QApplication app (argc, argv);
#ifndef Q_OS_ANDROID
  QString host = QString();
  QStringList args = app.arguments ();

  for (int i = 1; i < args.count (); i++)
    {
      QString arg = args.at (i);

      if (arg.at (0) == '-')
	{
	  switch (arg.at (1).toLatin1 ())
	    {
	    case 'h':
	    default:
	      usage ();
	    }
	}
    }
#else
  setenv ("QT_NECESSITAS_COMPATIBILITY_LONG_PRESS", "1", 1);
  // FIXME Need a long-press to show discard stack.
  //setenv ("QT_ANDROID_ENABLE_RIGHT_MOUSE_FROM_LONG_PRESS", "1", 1);
#endif

  app.setOrganizationName("Picnic");
  app.setApplicationName("QSkipBo");
  QSkipBo mainWin;
  mainWin.show();
  return app.exec();
}
