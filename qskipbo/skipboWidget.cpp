/*
    Copyright (C) 2013, 2014 Ben Kibbey <bjk@luxsci.net>

    This file is part of SkipBo

    SkipBo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    SkipBo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SkipBo.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "skipboWidget.h"

SkipBoWidget::SkipBoWidget (QWidget *parent) : QWidget (parent)
{
  ui.setupUi (this);
  ui.buildPile1->setCard (0, Card::BuildPile);
  ui.buildPile1->setId (1);
  ui.buildPile2->setCard (0, Card::BuildPile);
  ui.buildPile2->setId (2);
  ui.buildPile3->setCard (0, Card::BuildPile);
  ui.buildPile3->setId (3);
  ui.buildPile4->setCard (0, Card::BuildPile);
  ui.buildPile4->setId (4);

  ui.cb_discard->addItem (tr ("Player"));
  ui.cb_discard->addItem (tr ("Top"));
  ui.cb_discard->addItem (tr ("All"));
}

SkipBoWidget::~SkipBoWidget ()
{
}

void
SkipBoWidget::setBuildPile (int b, int c)
{
  if (b == 1)
    ui.buildPile1->setCard (c, Card::BuildPile);
  else if (b == 2)
    ui.buildPile2->setCard (c, Card::BuildPile);
  else if (b == 3)
    ui.buildPile3->setCard (c, Card::BuildPile);
  else if (b == 4)
    ui.buildPile4->setCard (c, Card::BuildPile);
}
